'use strict';

module.exports = (app) => {
  let users = require('../controllers/users.controller');

  app.post('/api/auth/signin', users.signin);
  app.post('/api/auth/signup', users.signup);
  app.post('/api/auth/signout', users.signout);
  app.post('/api/auth/refresh', users.refreshAccessToken);
  app.post('/api/auth/firebase_token', users.firebaseToken);

}
