 'use strict';

var debug = require('debug')('nkapsi'),
    passport = require('passport'),
    jwt = require('jsonwebtoken'),

    token = require('./users.token.controller'),
    User = require('../models/user.model');

exports.one = function(req, res) {
  debug(req.customer);

  return res.json({
    success: true,
    customer: req.customer
  });
}

exports.userById = function(req, res, next, id) {
  debug(id);

  User.findById(id).then(function(user, error) {
    debug(user);
    debug(error);

    if (error) {
      return;
    }

    // user.profile_image_url = "http://localhost:3000/" + user.profile_image_url;
    req.customer = user;
    req.user = user;
    next();
  });
}

exports.partnerById = function(req, res, next, id) {
  debug(id);

  Partner.findById(id).then(function(user, error) {
    debug(user);
    debug(error);

    if (error) {
      return;
    }

    user.profile_image_url = "http://localhost:3000/" + user.profile_image_url;
    req.customer = user;
    req.user = user;
    next();
  });
}
