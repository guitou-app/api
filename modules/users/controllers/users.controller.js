'use strict';

var _ = require('lodash');

module.exports = _.extend(
  require('./users.authentication.controller'),
  require('./users.profile.controller'),
  require('./users.crud.controller'),
  // require('./users.partners.controller')
);
