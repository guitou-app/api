'use strict';

var debug = require('debug')('Guitou-Auth'),
    passport = require('passport'),
    jwt = require('jsonwebtoken'),
    uuidv4 = require('uuid/v4'),

    config = require('../../../config/config'),
    // errorHandler = require('../errors.controller'),

    token = require('./users.token.controller'),
    User = require('../models/user.model');

// const {OAuth2Client} = require('google-auth-library');
// const client = new OAuth2Client(config.google.clientID);

exports.signup = function(req, res) {
  delete req.body.roles;

  debug("Sign UP-------------------------");

  var data = req.body;
  debug(data);

  // Init user and add missing fields
  debug("Usssssssssssssssssssssssssssssssssss");
  // data['username'] = data[]
  var user = new User(data);
  debug(user);

  user.provider = 'local';
  user.displayName = data.name; // user.firstName + ' ' + user.lastName;
  user.username = data.phone;
  debug(user);

  User.findOne({
    $or: [
        { email: user.email },
        { username: user.username }
    ]
    // email: user.email
  }, function(err, userFound) {

    if (!err && userFound) {
      return res.status(400).send({
        message: 'An user with this email or phone number already exists'
      });
    } else if (err) {
      return res.status(400).send({
        message: 'An error occured please retry'
      });
    } else if (!userFound) {

      // If He wants to be keep connected
      user.refreshToken = uuidv4();

      // Then save the user
      user.save(function (err) {
        if (err) {
          debug(err);
          return res.status(422).send({
            message: 'Error when saving user data'
          });
        } else {
          // Remove sensitive data before login
          user.password = undefined;
          user.salt = undefined;

          token.createToken(user.toObject(), function(err, token) { // res, err, token) {
            if (err) {
              return res.status(400).send(err);
            }

            res.status(201).json({
              user: user,
              access_token: token,
              refresh_token: user.refreshToken
            });
          });
        }
      });
    }
  });
}

/**
 * Signin after passport authentication
 */
exports.signin = function (req, res, next) {
  debug("Signin body");
  debug(req.body);

  const { username, password } = req.body;

  User.findUniquePhoneOrEmail(username.trim(), username.trim(), '', function(err, user) {
    if (err) {
      return done(err);
    }
    debug("Into");
    debug(user);
    debug(user.authenticate(password));
    if (!user || !user.authenticate(password)) {
      debug("Invalllllllllllliiiiiiiiiiiddddddddddddd up");
      debug(user);
      return res.status(422).send({
        message: "Invalid username or password"
      });
    }

    // return done(null, user);
    debug("AFT ERRR INNNOTTTOOOO");
    debug(user);
    user.refreshToken = uuidv4();

    // Then save the user
    user.save(function (err) {
      if (err) {
        debug(err);
        return res.status(422).send({
          message: 'Error when saving user data'
        });
      } else {
        // Remove sensitive data before login
        user.password = undefined;
        user.salt = undefined;

        debug("OK.....................................");
        debug(user);
        
        token.createToken(user.toObject(), function(err, token) { // res, err, token) {
          if (err) {
            return res.status(400).send(err);
          }
          res.status(201).send({
            user: user,
            access_token: token,
            refresh_token: user.refreshToken
          });
        });
      }
    });
    // return done(null, user, {});
  });

//   passport.authenticate('local', function (err, user, info) {
//     if (!user) {
//       // No user
//       debug("NNNNNOOOOOOOOO UUSSSERRRRR");
//       debug(err);
//       debug(user);
//       res.status(423).send('No user');
//     } else if (err) {
//       // User exists but wrong credentials
//       res.status(422);

//     /*if (err || !user) {
//       debug("info");
//       debug(info);
//       res.status(422).send(info);*/
//     } else {

//       // user.password = undefined;
//       // user.salt = undefined;
//       user.refreshToken = uuidv4();

//       // Then save the user
//       user.save(function (err) {
//         if (err) {
//           debug(err);
//           return res.status(422).send({
//             message: 'Error when saving user data'
//           });
//         } else {
//           // Remove sensitive data before login
//           user.password = undefined;
//           user.salt = undefined;

//           debug("OK.....................................");
//           debug(user);
//           token.createToken(user.toObject(), function(err, token) { // res, err, token) {
//             if (err) {
//               return res.status(400).send(err);
//             }
// 1
//             res.status(201).send({
//               user: user,
//               access_token: token,
//               refresh_token: user.refreshToken
//             });
//           });
//         }
//       });
//     }
//   })(req, res, next);
};

exports.signout = function(req, res) {
  debug(req.headers);

  token.expireToken(req.headers, function(err, success) {
    debug(err);
    debug(success);

		if (err) {
			// logger.error(err.message);

			return res.status(401).send(err.message);
		}

		if (success) {
			delete req.user;
      debug("IS SUCCESSFUL...");
			// return res.sendStatus(200);
      return res.status(200).json({
        // user: credentials,
        // access_token: token,
        // refresh_token: user.refreshToken
      });
		} else {
			return res.sendStatus(401);
		}
	});
}

exports.refreshAccessToken = function(req, res) {
  debug('refreshAccessToken');
  debug(req.body);
  debug('Before');

  User.findOne({ refreshToken: req.body.refresh_token }).then(function(user, error) {
    // debug(user);
    // debug(error);
    // debug("After");

    user.refreshToken = uuidv4();

    // Then save the user
    user.save(function (err) {
      if (err) {
        debug(err);
        return res.status(422).send({
          message: 'Error when saving user data'
        });
      } else {
        // Remove sensitive data before login
        user.password = undefined;
        user.salt = undefined;

        // debug(user);
        debug("createToken");
        token.createToken(user.toObject(), function(err, token) { // res, err, token) {
          if (err) {
            return res.status(400).send(err);
          }

          debug(user);
          res.status(201).json({
            user: user,
            access_token: token,
            refresh_token: user.refreshToken
          });
        });
      }
    });
  });
}

exports.firebaseToken = async (req, res) => {
  var token = req.body.token;
  var customer = req.body.customer;
  var emei = req.body.emei;

  debug("FIREBASE TOKEN");
  debug(token);
  debug(customer);
  debug(emei);

  try {
    let user = await User.findOneAndUpdate({_id: customer}, { firebaseToken: token }, { new: false });
    debug(user);

    if (!user) {
      return res.status(404).send(false);
    } else {
      return res.status(200).send(true);
    }
  } catch(err) {
    return res.status(500).send(err);
  }

  /*token.verifyToken(jwt, function(err, userData) {
    if (err) {
      debug(err);

      return;
    }

    debug(userData);

    User.findByIdAndUpdate(
      userData.id,
      { firebaseToken: token },
      function(err, user) {
        if (err) {
          return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
          });
        }

        debug(user);
        return res.status(201).json(true);
      }
    );
  });*/

  /*if (jwt) {
      token.verifyToken(jwt, function(err, userData) {
        if (err) {
          debug(err);

          return;
        }

        debug(user);

        User.findByIdAndUpdate(
          userData.id,
          { firebaseToken: token },
          function(err, user) {
            if (err) {
              return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
              });
            }

            debug(user);
            return res.status(201).json(true);
          }
        );
      });
  } else {
    var data = {
      token: token,
      emei: emei
    };
    var firebase = new Firebase(data);
    firebase.save(function(err, firebase) {
      if (err) {
        debug(err);

        return false;
      }

      debug(firebase);
      return true;
    })
  }*/

}

//
// exports.oauthCallToken = function (req, res, next) {
//   var strategy = req.params.strategy;
//   debug("OAUTH CALLL STRATEGGGGGYYYYYYYYYYY - " + strategy);
//
//   // Authenticate
//   passport.authenticate("facebook-token", {session: false})(req, res, next);
// };
//
// /**
//  * OAuth provider call
//  */
// exports.oauthCall = function (req, res, next) {
//   var strategy = req.params.strategy;
//   debug("OAUTH CALLL STRATEGGGGGYYYYYYYYYYY - " + strategy);
//
//   // Authenticate
//   passport.authenticate(strategy)(req, res, next);
// };
//
//
// /**
//  * OAuth callback
//  */
// exports.oauthCallback = function (req, res, next) {
//   var strategy = req.params.strategy;
//
//   debug("AUTH callback STRATEGGGGGYYYYYYYYYYY ::: " + strategy);
//
//   // info.redirect_to contains inteded redirect path
//   passport.authenticate(strategy, function (err, user, info) {
//     debug("OAUTH CALLBACK - ERRROOOR");
//     debug(err);
//     debug("OAUTH CALLBACK - USER \n");
//     debug(user);
//     debug("OAUTH CALLBACK - infoooo\n");
//     debug(info);
//
//     if (err) {
//       debug("OAUTH CALLBACK - ERRROOOR - 222222é");
//       // return res.redirect('/authentication/signin?err=' + encodeURIComponent(errorHandler.getErrorMessage(err)));
//       return res.send({
//         success: false,
//         message: encodeURIComponent(errorHandler.getErrorMessage(err))
//       });
//     }
//     if (!user) {
//       debug("OAUTH CALLBACK - !USER - 2222222222");
//       return res.redirect('/authentication/signin');
//     }
//
//     debug("OAUTH CALLBACK - USER - 2222222222");
//     token.createToken(user.toObject(), function(err, token) { // res, err, token) {
//       if (err) {
//         return res.status(400).send(err);
//       }
//
//       res.status(201).json({
//         user: user,
//         access_token: token,
//         refresh_token: user.refreshToken
//       });
//     });
//
//     /*req.login(user, function (err) {
//       if (err) {
//         return res.redirect('/authentication/signin');
//       }
//
//       return res.redirect(info.redirect_to || '/');
//     });*/
//   })(req, res, next);
// };
//
// /**
//  * Helper function to save or update a OAuth user profile
//  */
// exports.saveOAuthUserProfile = function (err, providerUserProfile, done) {
// // exports.saveOAuthUserProfile = function (req, res, next) {
//
//   debug("saveOAuthUserProfile-----------------------");
//   // debug(req);
//   // debug(req.user);
//   debug(providerUserProfile);
//
//   // var providerUserProfile = req.user;
//
//   // Setup info and user objects
//   var info = {};
//   var user;
//
//   // Set redirection path on session.
//   // Do not redirect to a signin or signup page
//   /*if (noReturnUrls.indexOf(req.session.redirect_to) === -1) {
//     info.redirect_to = req.session.redirect_to;
//   }*/
//
//   // Define a search query fields
//   var searchMainProviderIdentifierField = 'providerData.' + providerUserProfile.providerIdentifierField;
//   var searchAdditionalProviderIdentifierField = 'additionalProvidersData.' + providerUserProfile.provider + '.' + providerUserProfile.providerIdentifierField;
//
//   // Define main provider search query
//   var mainProviderSearchQuery = {};
//   mainProviderSearchQuery.provider = providerUserProfile.provider;
//   mainProviderSearchQuery[searchMainProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];
//
//   // Define additional provider search query
//   var additionalProviderSearchQuery = {};
//   additionalProviderSearchQuery[searchAdditionalProviderIdentifierField] = providerUserProfile.providerData[providerUserProfile.providerIdentifierField];
//
//   // Define a search query to find existing user with current provider profile
//   var searchQuery = {
//     $or: [mainProviderSearchQuery, additionalProviderSearchQuery]
//   };
//
//   // Find existing user with this provider account
//   debug("searchQuery");
//   debug(searchQuery);
//   User.findOne(searchQuery, function (err, existingUser) {
//     if (err) {
//       return done(err);
//     }
//     debug("User.findOne");
//     // debug(req.user);
//     debug(existingUser);
//     if (!existingUser) {
//       // var possibleUsername = providerUserProfile.username || ((providerUserProfile.email) ? providerUserProfile.email.split('@')[0] : '');
//       var possibleUsername = providerUserProfile.email;
//
//       User.findUniquePhoneOrEmail(possibleUsername, possibleUsername, null, function(err, localUser) { //function (availableUsername) {
//         debug("User.findUniquePhoneOrEmail : "); // + availableUsername);
//         debug(err);
//         debug(localUser);
//
//         if (!localUser) {
//           user = new User({
//             name: providerUserProfile.displayName,
//             firstName: providerUserProfile.firstName,
//             lastName: providerUserProfile.lastName,
//             // username: availableUsername,
//             displayName: providerUserProfile.displayName,
//             profileImageUrl: providerUserProfile.profileImageURL,
//             provider: providerUserProfile.provider,
//             providerData: providerUserProfile.providerData
//           });
//
//           // Email intentionally added later to allow defaults (sparse settings) to be applid.
//           // Handles case where no email is supplied.
//           // See comment: https://github.com/meanjs/mean/pull/1495#issuecomment-246090193
//
//           user.refreshToken = uuidv4();
//           user.email = providerUserProfile.email;
//
//           // And save the user
//           user.save(function (err, user) {
//
//             if (err) {
//               debug("EROOOOOOOOOOOOOOOOOOOOOOOORRRrrrrrrrr........ saving usssserr");
//
//               return done(err, user, info);
//             }
//
//             debug("Users..... savedddddd");
//             debug(err);
//             debug(user);
//
//             /*token.createToken(user.toObject(), function(err, token) { // res, err, token) {
//               if (err) {
//                 return res.status(400).send(err);
//               }
//
//               res.status(201).json({
//                 user: user,
//                 access_token: token,
//                 refresh_token: user.refreshToken
//               });
//             });*/
//             return done(err, user, info);
//           });
//         } else {
//           // Add the provider data to the additional provider data field
//           if (!localUser.additionalProvidersData) {
//             localUser.additionalProvidersData = {};
//           }
//
//           localUser.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;
//           // Then tell mongoose that we've updated the additionalProvidersData field
//           localUser.markModified('additionalProvidersData');
//
//           debug("Humm.......... markModified ... add");
//           debug(localUser);
//           // And save the user
//           localUser.save(function (err) {
//
//             // debug(req);
//             /*token.createToken(localUser.toObject(), function(err, token) { // res, err, token) {
//               if (err) {
//                 return res.status(400).send(err);
//               }
//
//               res.status(201).json({
//                 user: localUser,
//                 access_token: token,
//                 refresh_token: localUser.refreshToken
//               });
//             });*/
//             return done(err, localUser, info);
//           });
//         }
//
//
//       });
//     } else {
//       debug("Existing user..... already");
//       // return done(err, existingUser, info);
//       // next(); // (err, existingUser, info);
//       debug("Token------ creation");
//       // debug(req);
//
//       /*token.createToken(existingUser.toObject(), function(err, token) { // res, err, token) {
//         if (err) {
//           return res.status(400).send(err);
//         }
//
//         res.status(201).json({
//           user: existingUser,
//           access_token: token,
//           refresh_token: existingUser.refreshToken
//         });
//       });*/
//       debug(done.toString());
//
//       done(err, existingUser, info);
//       // done();
//     }
//   });
// };
//
// exports.oauthGoogleToken = async function (req, res, done) {
//
//   debug.log = console.log.bind(console);
//
//   debug("oauthGoogleToken------------------");
//   debug(req.body.token);
//   debug("Google Token..............");
//
//   // passport.authenticate('google-auth-library')
//
//   var accessToken = req.body.token;
//   const ticket = await client.verifyIdToken({
//     idToken: accessToken,
//     audience: config.google.clientID,  // Specify the CLIENT_ID of the app that accesses the backend
//     // Or, if multiple clients access the backend:
//     //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
//   })
//   debug(ticket);
//   const payload = ticket.getPayload();
//   const userid = payload['sub'];
//
//   var providerData = payload;
//   providerData.accessToken = accessToken;
//   // providerData.refreshToken = refreshToken;
//
//   var providerUserProfile = {
//     firstName: payload.given_name,
//     lastName: payload.family_name,
//     displayName: payload.name,
//     email: payload.email,
//     profileImageURL: payload.picture,
//     provider: 'google',
//     providerIdentifierField: 'sub',
//     providerData: providerData
//   };
//   debug(providerUserProfile);
//
//   req.user = providerUserProfile;
//
//   // If request specified a G Suite domain:
//   //const domain = payload['hd'];
//   module.exports.saveOAuthUserProfile(null, providerUserProfile, function(err, user, info) {
//
//     if (err) {
//       debug("TTTTHHHHHHHEREEE ISS ANN EROOORR");
//       debug(err);
//       debug(user);
//
//       return res.status(422).json({
//         message: "An error occured, please try later"
//       });
//     }
//
//     debug("Token creation");
//     debug(user);
//     debug("Uoooooooooooooooooooooooooooooo");
//     debug(info);
//     debug("IIIInnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
//     // debug(req);
//     // token.createToken(user.toObject(), function(err, accessToken) { // res, err, token) {
//     token.createToken(user, function(err, accessToken) { // res, err, token) {
//       if (err) {
//         return res.status(400).send(err);
//       }
//
//       return res.status(201).json({
//         user: user,
//         access_token: accessToken,
//         refresh_token: user.refreshToken
//       });
//     });
//   });
//   // return done(null, providerUserProfile);
// };
//
// exports.oauthFacebookToken = function(req, res) {
//   debug("oauthFacebookToken.....................");
//
//   passport.authenticate('facebook-token', {session: false}, function(err, user, info) {
//     debug("Token creation");
//     debug(user);
//     debug("Uoooooooooooooooooooooooooooooo");
//     debug(info);
//     debug("IIIInnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
//     // debug(req);
//
//     token.createToken(user.toObject(), function(err, accessToken) { // res, err, token) {
//       if (err) {
//         return res.status(400).send(err);
//       }
//
//       res.status(201).json({
//         user: user,
//         access_token: accessToken,
//         refresh_token: user.refreshToken
//       });
//     });
//   })(req, res)
// }
