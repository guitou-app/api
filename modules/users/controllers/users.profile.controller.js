'use strict';

/**
 * Module dependencies
 */
var debug = require('debug')('nkapsi'),
    error = debug('nkapsi:error'),

    _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  // errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  multer = require('multer'),
  crypto = require('crypto'),
  config = require(path.resolve('./config/config')),
  User = mongoose.model('User'),

  token = require('./users.token.controller');

/**
 * Update user details
 */
exports.update = function (req, res) {
  // Init Variables
  var user = req.user;

  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;

  // For security measurement do not use _id from the req.body object
  delete req.body._id;

  if (user) {
    // Merge existing user
    user = _.extend(user, req.body);
    user.updated = Date.now();
    user.displayName = user.firstName + ' ' + user.lastName;

    user.save(function (err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        req.login(user, function (err) {
          if (err) {
            res.status(400).send(err);
          } else {
            res.json(user);
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};

/**
 * Send User
 */
exports.me = async (req, res) => {
  debug("Profile - ME");
  debug(req.params);
  debug("MMMMMMMMMMeeeeeeeeeeeee-----------------------");
  try {
    let user = await User.findOne({ email: req.params.email });

    debug(user);
    user.salt = undefined;
    user.password = undefined;

    return res.json({
      success: true,
      user: user
    })
  } catch (err) {
    return res.status(500).send(err);
  }
}

/*exports.me = function (req, res) {
  debug("Profile - ME");
  debug(req.params);
  // console.log(req.user._doc);
  // User.findOne({ _id: req.user._id }, function(err, user) {
  User.findOne({ email: req.params.email }, function(err, user) {
    if (err) {
      res.status(400).json(err);
    } else {

      user.salt = undefined;
      user.password = undefined;

      res.json({
        success: true,
        user: user
      });
    }
  });
  // res.json(req.user);
};*/

/**
 * Update profile picture
 */
exports.changeProfilePicture = function (req, res) {
  var user = req.user;
  var existingImageUrl;
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      console.log('\nDESTINATION\n');
      console.log(file);

      //cb(null,  config.uploads.profileUpload)
      cb(null, 'modules/users/client/img/profile/uploads/');
    },
    filename: function (req, file, cb) {
      console.log('\nFILENAME\n');
      console.log(file);

      User.findById(req.user._id, function(err, user) {
        if (err) {
          res.status(400).json(err);
        } else {

          const key = crypto.pbkdf2Sync(
            req.user.phone + Date.now(),
            new Buffer(user.salt, 'base64'), 10000, 64
          ); //.toString('base64');

          cb(null, key.toString('hex') + '.png');
        }
      });
      // cb(null, file.filename + Date.now() + '.png'); // + path.extname(file.mimetype));


      // crypto.pseudoRandomBytes(16, function (err, raw) {
      //   cb(null, raw.toString(req.user.name + req.user.email + req.user.phone) + Date.now() + '.png'); // + mime.extension(file.mimetype));
      // });
    }
  });
  var upload = multer({ storage: storage }).single('picture');
  // var upload = multer(config.uploads.profileUpload).single('picture');
  var profileUploadFileFilter = require(path.resolve('./config/lib/multer')).profileUploadFileFilter;

  // Filtering to upload only images
  upload.fileFilter = profileUploadFileFilter;

  if (user) {
    existingImageUrl = user.profileImageURL; // + path.extname(file.originalname);

    console.log('\n' + existingImageUrl + '\n');

    uploadImage()
      .then(updateUser)
      // .then(deleteOldImage)
      .then(login)
      .then(function (result) {
        console.log('\nLAST THENNNNNN\n');
        console.log('\n' + result.userFound + '\n');
        console.log('\n' + result.token + '\n');
        res.json(result);
        //   {
        //   user: userFound,
        //   token: token
        // });
      })
      .catch(function (err) {
        console.log(err);
        res.status(422).send(err);
      });
  } else {
    res.status(401).send({
      message: 'User is not signed in'
    });
  }

  function uploadImage () {
    return new Promise(function (resolve, reject) {
      upload(req, res, function (uploadError) {
        if (uploadError) {
          console.log('\n' + uploadError + '\n');
          reject(errorHandler.getErrorMessage(uploadError));
        } else {
          resolve();
        }
      });
    });
  }

  function updateUser () {
    return new Promise(function (resolve, reject) {
      User.findById(user._id, function(err, userFound) {
        if (err) {
          reject(errorHandler.getErrorMessage(err));
        } else {
          console.log('\n' + userFound + '\n');

          console.log('\nORIGINAL NAME : \t' + req.file.originalname + '\t' + path.extname(req.file.originalname));
          console.log(req.file);
          console.log('\n');

          userFound.profileImageURL = config.uploads.profileUpload.dest + req.file.filename; // + path.extname(req.file.originalname);
          userFound.defaultProfileImageURL = false;

          console.log('\n' + userFound + '\n');

          userFound.save(function (err, theuser) {
            if (err) {
              reject(err);
            } else {
              resolve(theuser);
            }
          });
        }
      });
    });
  }

  function deleteOldImage () {
    return new Promise(function (resolve, reject) {

      if (existingImageUrl !== User.schema.path('profileImageURL').defaultValue) {
        console.log('\n' + existingImageUrl + '\n');
        fs.unlink(existingImageUrl, function (unlinkError) {
          if (unlinkError) {
            console.log(unlinkError);
            reject({
              message: 'Error occurred while deleting old profile picture'
            });
          } else {
            resolve();
          }
        });
      } else {
        resolve();
      }
    });
  }

  function login (userFound) {
    console.log('\nINTO LOOOOOGGGGGIIINNGGG\n');
    console.log('\n' + userFound + '\n');

    return new Promise(function (resolve, reject) {
      userFound.password = undefined;
      userFound.salt = undefined;

      token.createToken(userFound.toObject(), function(res, err, token) {

				if (err) {
					return res.status(400).send(err);
				}
        console.log('\n\n');
        console.log(token);
        console.log(err);
        console.log('\n\n');

        // resolve(userFound, token);
        resolve({
          user: userFound,
          token: token
        });
				// res.status(201).json({
				// 	user: user,
				// 	token: token
				// });
			}.bind(null, res));

      // req.login(user, function (err) {
      //   if (err) {
      //     res.status(400).send(err);
      //   } else {
      //     resolve();
      //   }
      // });
      // resolve();
    });

    function success(userFound, token) {
      console.log('\nLAST THENNNNNN\n');
      console.log('\n' + userFound + '\n');
      console.log('\n' + token + '\n');
      res.json({
        user: userFound,
        token: token
      });
    }
  }
}

exports.beCourier = (req, res) => {
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/be_couriers/')
    },
    filename: function (req, file, cb) {
      debug("fileNAME");
      debug(file);
      debug(req.user);

      cb(null, req.user.email + '-' + file.fieldname + '-' + Date.now() + '.png');
    }
  });
  var upload = multer({ storage: storage }).fields([{ name: 'nic1', maxCount: 1 }, { name: 'nic2', maxCount: 1 }]); 

  debug("\nBE COURIER\n");

  function uploadNIC () {
    return new Promise(function (resolve, reject) {
      upload(req, res, function (uploadError) {
        if (uploadError) {
          debug('\n' + uploadError + '\n');
          reject(uploadError);
        } else {
          resolve();
        }
      });
    });
  }

  //debug(req);
  debug('\n\n');

  /*upload(req, res, function (uploadError) {
    debug("UPLOAD FUNCTION...");
    debug(uploadError);

    if (uploadError) {
      debug(uploadError);
      return res.status(422).send(uploadError);
    } 

    debug("\nSUCCESS 1\n");
    debug(req.body);

    return res.status(201);
  });*/

  uploadNIC()
    .then(function() {
      debug("\nSUCCESS 1\n");
      debug(req.body);
      debug(req.files['nic1']);
      debug(req.files['nic2']);

      return res.status(201);
    })
    .catch(function (err) {
      console.log(err);
      res.status(422).send(err);
    });
}