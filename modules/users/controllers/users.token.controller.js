'use strict'

var jwt = require('jsonwebtoken');

var debug = require('debug')('Guitou');
var path = require('path'),
    config = require(path.resolve('./config/config')),
    redis = require(path.resolve('./config/lib/redis'));
// var config = require('../../config/config');

var extractToken = function(headers) {
    if (headers == null) {
        throw new Error('headers is null');
    }
    if (headers.authorization == null) {
        throw new Error('authorization header is null');
    }

    var authorization = headers.authorization;
    var authArr = authorization.split(' ');
    if (authArr.length !== 2) {
        throw new Error('authorization header value is not of length 2');
    }

    var token = authArr[1];
    try {
        jwt.verify(token, config.jwt.secret);
    } catch(err) {
        throw new Error('the token is not valid');
    }

    return token;
};

var createToken = function(payload, cb) {
    var ttl = config.jwt.expiration;

    if (payload != null && typeof payload !== 'object') {
        return cb(new Error('payload is an object'));
    }
    if (ttl != null && typeof ttl !== 'number') {
        return cb(new Error('ttl is not a valid number'));
    }

    debug('\n\n *** CREATE TOKEN JWT --- ', config.jwt.secret);
    var token = jwt.sign(payload, config.jwt.secret, { expiresIn: config.jwt.expiration, algorithm: 'HS256' });

    // stores a token with payload data for a ttl period of time
    redis.setex(token, ttl, JSON.stringify(payload), function(token, err, reply) {
    //redis.set(token, JSON.stringify(payload), 'EX', ttl, function(token, err, reply) {
      // console.log(token);
      // console.log(err);
      // console.log('\n\n');
      //
      // console.log(cb);

        if (err) {
            return cb(err);
        }
        // console.log("REPLY ---------- " + token);
        if (reply) {
          // console.log('\nINTO REPLY --------');
          cb(null, token);
        } else {
            cb(new Error('token not set in redis'));
        }
    }.bind(null, token));
};

var expireToken = function(headers, cb) {
    try {
        var token = extractToken(headers);
        debug("Expire Token - " + token);

        if (token == null) {
            return cb(new Error('token is null'));
        }

        // delete from Redis
        redis.del(token, function(err, reply) {
            if (err) {
                return cb(err);
            }

            if (!reply) {
                return cb(new Error('token not found'));
            }
            return cb(null, true);
        });
    } catch (err) {
        return cb(err);
    }
};

var verifyToken = function(headers, cb) {
    try {
        var token = extractToken(headers);

        if (token == null) {
            return cb(new Error('token is null'));
        }

        // gets the associated data of token
        redis.get(token, function(err, userData) {
            if (err) {
                return cb(err);
            }
            if (!userData) {
                return cb(new Error('token not found'));
            }

            return cb(null, JSON.parse(userData));
        });
    } catch (err) {
        return cb(err);
    }
};

var checkToken = function(req, res, next) {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  if (token.startWith('Bearer ')) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }

  if (token) {
    jwt.verify(token, config.jwt.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
        req.user = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
}

module.exports = {
  extractToken: extractToken,
  createToken: createToken,
  expireToken: expireToken,
  verifyToken: verifyToken,
  checkToken: checkToken
};
