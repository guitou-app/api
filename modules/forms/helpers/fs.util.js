var fs = require('fs'),
    path = require('path'),
    debug = require('debug')('fs-util')
;

const rmdir = (dir) => {
  var list = fs.readdirSync(dir);
  for (var i = 0; i < list.length; i++) {
    var filename = path.join(dir, list[i]);
    var stat = fs.statSync(filename);

    if (filename == '.' || filename == '..') {
      // Let's pass these folder
    } else if (stat.isDirectory()) {
      // Recursively callback rmdir
      rmdir(filename);
    } else {
      fs.unlinkSync(filename);
    }
  }
  fs.rmdirSync(dir);
}

const copyDir = (src, dest) => {
  if (!fs.existsSync(dest)) {
    fs.mkdirSync(dest); //, 0755);
  }

  var files = fs.readdirSync(src);
  for (var i = 0; i < files.length; i++) {
    var current = fs.lstatSync(path.join(src, files[i]));

    if (current.isDirectory()) {
      debug("isDirectory ... ", files[i]);
      copyDir(path.join(src, files[i]), path.join(dest, files[i]));
    } else if (current.isSymbolicLink()) {
      var symlink = fs.readlinkSync(path.join(src, files[i]));
      fs.symlinkSync(symlink, path.join(dest, files[i]));
    } else {
      fs.writeFileSync(path.join(dest, files[i]), fs.readFileSync(path.join(src, files[i])));
      // try {
      //   fs.writeFileSync(path.join(dest, files[i]), fs.readFileSync(path.join(src, files[i])), { flag: "r+"});
      // } catch(e) {
      //   fs.writeFileSync(path.join(dest, files[i]), fs.readFileSync(path.join(src, files[i])));
      // }

    }
  }
}

const copy = (src, dest) => {
  var oldFile = fs.createReadStream(src);
  var newFile = fs.createWriteStream(dest);

  oldFile.pipe(newFile);
}

module.exports = {
  rmdir : rmdir,
  copy: copy,
  copyDir: copyDir
}
