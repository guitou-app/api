var amqp = require('amqplib');
var debug = require('debug')("Rabbitmq:save");
var config = require('../../../config/config');

debug("AMQP URI : ", config.rabbitmq.uri);
console.log("AMQP URI : ", config.rabbitmq.uri);

var q = 'guitou.data';
exports.createClient = () => amqp.connect(config.rabbitmq.uri) //('amqp://guitou:guitou-dev@192.168.1.102')
  .then(conn => {
    return conn.createChannel();
  }, reason => {
    console.log("\nFAILED TO CONNECT FROM RABBITMQ.save.js ... ");
    console.log(reason);
    console.log('\n');
  }) // create channel
  .then(ch => {
    // console.log(ch);
    debug("AMQPLib (Guitou.data) connectd to " + config.rabbitmq.uri); //" amqp://guitou:guitou-dev@192.168.1.102");
    console.log("AMQPLib (Guitou.data) connectd to " + config.rabbitmq.uri); //" amqp://guitou:guitou-dev@192.168.1.102");
    ch.assertQueue(q, {
      durable: true
    });
    return ch;
  })
  .catch(console.warn);
