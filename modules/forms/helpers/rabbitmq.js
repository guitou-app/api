// const amqp = require('amqplib/callback_api');
const amqp = require('amqplib');
const debug = require('debug')("guitou:rabbitmq");
const config = require('../../../config/config');

let connection, channel;

const init = async () => {
  connection = await amqp.connect(process.env.RABBITMQ_URL || 'amqp://localhost')
  channel = await this.connection.createChannel()
}

const publishToQueue = async (queue, msg) => {
  if (!connection) {
    await init();
  }
  await channel.assertQueue(queue, {durable: true});
  channel(queueName, buffer, {
    persistent: true
  });
}
 
module.exports = {
  init,
  publishToQueue
}
// let ch = null;
// amqp.connect(config.rabbitmq.uri, function (err, conn) {
//    conn.createChannel(function (err, channel) {
//       ch = channel;
//    });
// });

// exports.publishToQueue = async (queueName, data) => {
//   const buffer = Buffer.from(data);
//   ch.sendToQueue(queueName, buffer, {
//     persistent: true
//   });
// }


// process.on('exit', (code) => {
//   ch.close();
//   console.log(`Closing rabbitmq channel`);
// });

