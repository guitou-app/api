'use strict';

var debug = require('debug')('guitou:forms.controller'),
    mongoose = require('mongoose'),
    fs = require('fs'),
    path = require('path'),
    crypto = require('crypto');
const { waterfall } = require('async'); //.waterfall;

const { exec, execFile, spawn } = require('child_process');

var Form = mongoose.model('Form'),
    Xorm = mongoose.model('Xorm'),

    fsUtil = require('../helpers/fs.util'),

    token = require('../../users/controllers/users.token.controller');


var hashPassword = function (salt, password) {
  if (salt && password) {
    return crypto.pbkdf2Sync(password, new Buffer(salt, 'base64'), 10000, 64, 'sha1').toString('base64');
  } else {
    return password;
  }
};


exports.save = async (req, res) => {
  let status = 200,
      success = true;
  let body = req.body;
  debug('Form CREATE');
  debug(body);

  const { xormId } = req.params;

  let form = new Form(body);
  form.author = req.user._id;
  
  try {
    await form.save();
    let xormOne = await Xorm.findById(params.xormId);
      xormOne.forms = [...xormOne.forms, form._id];
      if (!xormOne.settings.forms.primary) {
        xormOne.settings.forms.primary = form._id
      }
    await xormOne.save();
  } catch (e) {
    debug("Error when saving XORM");
    debug(e);
    form = undefined;

    status = 400;
    success = false;
  }

  return res.status(status).json({
    success: success,
    xorm:  form
  })
}

exports.fetchAll = async function(req, res) {
  var allForms = [];
  var success = true;
  var status = 202;

  debug("FORMS ... fecthAll() --- ", req.params.xormId);
  debug(req.user);

  try {
    // allForms = await Form.find({ author: req.user._id, xorm: req.params.xormId });
    // allForms = await Form.find({ xorm: req.params.xormId });
    const xormOne = await Xorm.findById(req.params.xormId).populate('forms');
    debug('XORM ONE');
    debug(xormOne);
    allForms = xormOne.forms.map(f => {
      debug(f._id, xormOne.settings.forms.primary == f.id);

      return {
        ...f.toObject(),
        level: xormOne.settings.forms.primary == f.id ? 'primary' : 'secondary'
      }
    });

  } catch(e) {
    debug("Error when fetching forms....");
    debug(e);

    status = 404;
    allForms = [];
    success = false;
  }

  return res.status(status).json({
    success: success,
    data: allForms
  });
}

exports.fetchOne = async function(req, res) {
  var { xormId, formId} = req.params;
  var formOne = {};
  var success = true;

  try {
    const xormOne = await Xorm.findOne(
      {
        $and: [
          { _id: xormId },
          { forms: formId }
        ]
      }
    ).populate('forms');
    formOne = xormOne.forms.find(f => f._id == formId);
    
    const { primary, pKeyOrigin } = xormOne.settings.forms;
    formOne = {
      ...formOne.toObject(),
      level: primary == formId ? 'primary' : 'secondary',
      pKeyOrigin    
    }
    // formOne['level'] = primary == formId ? 'primary' : 'secondary';
    // formOne['primaryKeyOrigin'] = primaryKeyOrigin;

      // (req.params.xormId).populate('forms')
    // formOne = await Form.findById(formId);
    debug(formOne);
  } catch (e) {
    debug("Error when fetch one form - " + formId);
    debug(e);

    formOne = {}
    success = false;
  }

  return res.json({
    success: success,
    data: formOne
  });
}

exports.update = function(req, res) {
  var formId = req.params.formId;
  var body = JSON.parse(JSON.stringify(req.body));
  debug("Form. Update");
  debug(body);
  debug("\n");
  var set = {
    [req.body.toUpdate]: req.body.values
  }
  debug(set);

  Form.findOneAndUpdate({ _id: formId }, {
    "$set": set
  }, function(err, doc) {
    debug(err);
    if (!err && doc) {
      debug(doc);

      return res.status(202).json({
        success: true
      });
    } else {
      return res.status(403).json({
        success: false
      });
    }
  })
}

exports.fetchAllCollaborators = async (req, res) => {
  debug("Fetch Collaborators...");
  var formId = req.params.formId;

  var formOne = {};
  var success = true;
  var collaborators = undefined;

  try {
    formOne = await Form.findById(formId);
    collaborators = formOne.collaborators;
  } catch (e) {
    debug("Error when fetch one form - " + formId);
    debug(e);

    formOne = {}
    success = false;
  }

  return res.json({
    success: success,
    data: collaborators
  });
}

exports.addCollaborator = async (req, res) => {
  debug("addCollaborator");
  var formId = req.params.formId;
  var collaborator = req.body;

  // collaborator.password = this.hashPassword(collaborator.password);
  // if (collaborator.salt && collaborator.password) {
    //   collaborator.password = crypto.pbkdf2Sync(collaborator.password, new Buffer(collaborator.salt, 'base64'), 10000, 64, 'sha1').toString('base64');
    // } else {
      //   collaborator.password = collaborator.password;
      // }

  Form.findById(formId).then(form => {
    collaborator.salt = crypto.randomBytes(16).toString('base64');
    collaborator.password = hashPassword(collaborator.salt, collaborator.password);
    form.collaborators.push(collaborator);

    return form.save()
  }).then(form => {

    return res.json({
      success: true,
      data: form.collaborators[form.collaborators.length - 1]
    });
  }).catch(err => {
    debug(err);

    return res.json({
      success: false,
      err: err
    });
  });
}

exports.removeCollaborator = async (req, res) => {
  debug("addCollaborator");
  var formId = req.params.formId;
  var collaboratorId = req.params.collaboratorId;

  Form.findById(formId).then(form => {
    form.collaborators.id(collaboratorId).remove(); // push(collaborator);

    return form.save();
  }).then(form => {

    return res.json({
      success: true,
      // data: form.collaborators[form.collaborators.length - 1]
    });
  }).catch(err => {
    debug(err);

    return res.json({
      success: false,
      err: err
    });
  });
}

exports.collaboratorAuth = async (req, res) => {
  debug("collaboratorAuth");
  var formId = req.params.formId;
  var credentials = req.body;
  var collaborator;

  debug(credentials);
  // if (credentials.p)
  Form.findById(formId).then(form => {
    collaborator = form.collaborators.find(collaborator => collaborator.email == credentials.username || collaborator.username == credentials.username); // push(collaborator);

    if (!collaborator) {
      return res.status(423).json({
        success: false,
        err: "No collaborator"
      });
    }

    if (collaborator.password == hashPassword(collaborator.salt, credentials.password)) {
      collaborator["password"] = undefined;
      collaborator["salt"] = undefined;
      var data = collaborator.toObject();
      data.form = formId;

      token.createToken(data, function(err, token) {
        if (err) {
          return res.status(400).json({
            success: false,
            err:err
          });
        }

        res.status(201).json({
          success: true,
          data: data,
          access_token: token
        });
      });
    } else {
      return res.status(403).json({
        success: false,
        err: "username or password is not good"
      });
    }
  })
}

exports.generateMobileApp = async (req, res) => {
  var formId = req.params.formId;
  var theCurrentForm = undefined;
  var status = undefined;
  var success = true;
  var error = undefined;
  const baseDir = '../Customers/generated';

  // TODO: Fetch form information from DB
  try {
    theCurrentForm = await Form.findById(formId);
  } catch (e) {
    debug("Error when fetching form information");
    debug(e);

    error = e;
    success = false;
    status = 404;
  }

  if (typeof(theCurrentForm) == 'undefined') {
    return res.status(status).json({
      success: success,
      error: error
    })
  }

  debug("THE CURRENT FORM");
  debug(theCurrentForm);

  debug("\nDIRNAME.... ", __dirname);
  var appName = theCurrentForm.title.split(" ").join("");
  var appPath = path.join(__dirname,'../../../../Customers/generated', appName);
  var appTemplatePath = path.join(__dirname, '../../../../Customers/generated/TemplateQuestionnaire');
  debug(appPath);

  // TODO: Check if a folder with the form Title exists, if so delete it
  if (fs.existsSync(appPath)) {
    // TODO: Delete the folder
    fsUtil.rmdir(appPath);
    debug("DELETE ... ", appPath);
  }

  // TODO: Create the App folder
  fs.mkdirSync(appPath); //, 0755);
  debug("Mkdir ... ", appPath);

  // TODO: Copy the BoilerPlate folder : into a new folder
  fsUtil.copyDir(appTemplatePath, appPath);
  debug("Copied ... ", appTemplatePath, appPath);

  // TODO: Update the form.json
  fs.writeFileSync(path.join(appPath, 'src/Helpers/forms.json'), JSON.stringify(theCurrentForm, null, 2));
  debug("Update the form.json");

  // TODO: Update the application name
  var appJsonContent = {
    "name": appName,
    "displayName": theCurrentForm.title
  }
  fs.writeFileSync(path.join(appPath, 'app.json'), JSON.stringify(appJsonContent, null, 2));
  debug("Update the app.json");

  // TODO: Update package.json. 1- Read all the file; 2- Change "name" and "version" to the appName and formVersion; 3- Write it back

  // TODO: Generate the APK release: All the console must be save into the logs folders. The file name : {username}_generate_apk_{date}.log
  debug(process.env.PATH);
  // var env = getEnvKeyValuePairsSomeHow();

  waterfall([
    // TODO: Set the working Directory cLI to appPath
    // function(callback) {
    //   debug("CD .. ", appPath);
    //
    //   // execFile('cd ' + appPath, [], (error, stdout, stderr) => {
    //   execFile('pwd', [], (error, stdout, stderr) => {
    //     if (error) {
    //       // debug(error);
    //       throw error;
    //       callback(error);
    //     }
    //     debug(stdout);
    //     debug(stderr);
    //
    //     callback(null)
    //   })
    // },

    // TODO: Run the CLI command to install packagees: npm install (Think of copying the project with the node_modules folder)
    // function(callback) {
    //
    // }

    // TODO: Run the CLI Command to generate the APK: cd android && ./gradlew assembleRelease
    function(callback) {
      // debug('CD android...');
      debug('CHMOD gradlew...');

      // execFile('cd', ['android'], (error, stdout, stderr) => {
      //   if (error) {
      //     throw error
      //     callback(error);
      //   }
      //   debug(stdout);
      //   debug(stderr);
      //
      //   callback(null)
      // })
      fs.chmod(path.join(appPath, 'android/gradlew'), 0o755, (error) => {
        if (error) {
          throw error;
          callback(error);
        }

        callback(null);
      })
    },

    function(callback) {
      debug("RUN ./gradlew assembleRelease", path.join(appPath, 'android'));

      const gradlew = spawn('bash', ['gradlew', 'assembleRelease'], { cwd: path.join(appPath, 'android') });

      var errorData = "";
      gradlew.stdout.on('data', (data) => {
        debug(`stdout: ${data}`);
      });

      gradlew.stderr.on('data', (data) => {
        debug(`stderr: ${data}`);
        errorData += data;
      });

      gradlew.on('close', (code) => {
        debug(`\nClose process exited with code ${code}`);
        if (code == 1) {
          callback(errorData);
        } else {
          callback(null);
        }
      });
      // execFile('bash', ['gradlew', 'assembleRelease'], { cwd: path.join(appPath, 'android') }, (error, stdout, stderr) => {
      //   debug(stdout);
      //   debug(stderr);
      //   if (error) {
      //     throw error
      //     callback(error);
      //   }
      //
      //   callback(null)
      // })
    },

    // TODO: CLI Command: Rename the apk file : FormTitle + CurrentDate
    // TODO: CLI Command: Move the APK File to the download directory
    function(callback) {
      debug("CP APK File...........");

      var apkName = appName + '_' + Date.now;
      execFile('cp', ['app/build/outputs/apk/release/apk-release', './' + apkName], (error, stdout, stderr) => {
        if (error) {
          throw error
          callback(error);
        }
        debug(stdout);
        debug(stderr);

        callback(null)
      })
    }


  ], function(err, result) {
    debug("AAASSSYYYNNNNNNNCC ---- ERR RESULT");
    debug(err);
    debug(result);

  });





  // TODO: All the result

  // TODO: Send the APK back to the user or a link to download it
}

exports.download = async (req, res) => {

  var formId = req.params.formId;
  var formOne = {};
  var success = true;

  debug("\nDownload XORMS");

  try {
    formOne = await Form.findById(formId);
    const xormOne = await Xorm.findById(formOne.xorm);
    const { primary, pKeyOrigin } = xormOne.settings.forms;
    formOne = {
      ...formOne.toObject(),
      level: primary == formId ? 'primary' : 'secondary',
      pKeyOrigin
    }
    console.log(formOne);
  } catch (e) {
    debug("Error when fetch one form - " + formId);
    debug(e);

    formOne = {}
    success = false;
  }

  return res.json({
    success: success,
    data: formOne
  });
}
