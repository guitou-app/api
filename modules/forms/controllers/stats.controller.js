'use strict';

var debug = require('debug')('guitou:dashboard.controller'),
    mongoose = require('mongoose'),
    Dashboard = mongoose.model('Dashboard'),
    Form = mongoose.model('Form'),
    Data = mongoose.model('Data');

exports.fetchOne = async function(req, res) {
  const { xormId } = req.params;
  var dashboards = [];
  var success = true;

  debug("Dashboard fetchOne - " + xormId);
  try {
    // const forms = await Form.find({ xorm: xormId }, { _id: 1 });
    // debug(forms);
    // dashboardOne = await Dashboard.findOne({ formId: formId });
    dashboards = await Dashboard.find({ xorm: xormId }); // form: { $in: forms } });
    debug(dashboards);
  } catch (e) {
    debug("Error when fetch Dashboard - " + formId);
    debug(e);

    // dashboardOne = {}
    dashboards = [];
    success = false;
  }

  return res.json({
    success: success,
    data: dashboards
  });
}

exports.initChart = async (req, res) => {
  let formId = req.params.formId;

  let varis = [];
  // [
  //   { v: "section_0__0", t: "string" },
  //   { v: "section_0__1", t: "string" },
  //   { v: "section_0__2", t: "single_choice" }
  // ]

  let currentForm = await Form.findById(formId);
  for (const [section, values] of Object.entries(currentForm.form)) {
    let { questions } = values;
    debug(section);
    debug(questions);
    for (const [varid, details] of Object.entries(questions)) {
      varis.push({
        section: section,
        varid: varid,
        ...details
      })
    }
  }

  let facetsPipeline = {};
  for(let i=0; i < varis.length; i++) {
    let { varid, section } = varis[i];

    let stages = [
      {
        $project: {
          "variable": `$values.${section}.${varid}`
        }
      }
    ];
    if (varis[i].type == "single_choice") {
      stages.push({
        $group: {
          "_id": "$variable",
          "ratas": {
            $sum: 1
          }
        }
      });

      stages.push({
        $project: {
          "_id": 0,
          "key": "$_id",
          "value": "$ratas"
        }
      });

      // stages.push({
      //   $bucket: {
      //     groupBy: "$variable",
      //     boundaries: [0, 22],
      //     default: "others",
      //     output: {
      //        "count": { $sum: 1 },
      //        "ratas": { $push: "$variable" }
      //     }
      //   }
      // })
    } else if (varis[i].type == "string") {
      stages.push({
        $group: {
          "_id": null,
          "ratas": {
            $push: "$variable"
          }
        }
      });
      stages.push({
        $project: {
          "ratas": 1,
          "_id": 0
        }
      });
      // stages.push({
      //   $arrayElemAt: {}
      // })
    }

    facetsPipeline[varid] = stages;
  }


  let result = await Data.aggregate([
    {
      $match: {
        "form": "5d66fb5e13961e3f6ec7ec82"
      }
    },
    {
      $facet: facetsPipeline
    }
  ]);

  debug(result);
  let charts = [];
  for(let i=0; i < varis.length; i++) {
    let varid = varis[i];
    let ratas, chartType;

    if (varid.type == 'string') {
      ratas = result[0][varid.varid][0].ratas;
      chartType = 'list';
    } else if (varid.type == 'single_choice') {
      ratas = result[0][varid.varid];
      chartType = 'bar';
    }

    debug(ratas);
    charts.push({
      chart: chartType,
      variable: varid.varid,
      question: varid,
      ratas: ratas
    });
  }

  // let dashboard = Dashboard({
  //   formId: formId,
  //   charts: charts
  // });
  // let resultSave = await dashboard.save();
  let dashboard = await Dashboard.findOneAndUpdate(
    { formId: formId },
    {
      $push: {
        charts: charts
      }
    },
    {
      new: true,
      upsert: true
    }
  );

  res.json({
    success: true,
    data: dashboard // charts
  });
}

exports.addChart = async (req, res) => {
  var xormId = req.params.xormId;
  let data = req.body;
  const { formId } = data;
  delete data.formId;

  var chart = await Dashboard.findOneAndUpdate(
    // { xorm: xormId },
    { form: formId, xorm: xormId },
    {
      $push: {
        charts: data
      },
      // $set: {
      //   form: formId
      // }
    },
    {
      new: true,
      upsert: true
    }
  );

  res.json({
    data: chart,
    success: true
  });
}

exports.deleteChart = async (req, res) => {
  debug('\nDelete Chart');
  const { xormId, chartId } = req.params;

  Dashboard.findOne({ "xorm": xormId }).then((dashboard) => {
    debug('\nFindone.. Dashboard');
    debug(dashboard);
    dashboard.charts.id(chartId).remove();

    return dashboard.save();
  }).then((dashboard) => {
    return res.json({
      success: true,
      chartId: chartId
    });
  }).catch((error) => {
    debug(error);

    return res.json({
      success: false,
      error: error
    });
  })
}
