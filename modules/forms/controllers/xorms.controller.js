'use strict';

var fs = require('fs'),
    path = require('path');
var debug = require('debug')('guitou:xorms.controller'),
    mongoose = require('mongoose'),
    Xorm = mongoose.model('Xorm');


exports.fetchAll = async (req, res) => {
  
  var allXorms = [];
  var success = true;
  var status = 202;

  const { _id, email } = req.user;

  debug("xormS ... fecthAll()");
  debug(req.headers);
  debug(req.user);

  try {
    allXorms = await Xorm.find({ 
      "$or": [
        { author: _id }, 
        {"settings.collaborators": { $elemMatch: { "email": email, "status": "accepted" } } }
      ]
    }).populate({ path: 'author', select: 'displayName' });
  } catch(e) {
    debug("Error when fetching xorms....");
    debug(e);

    status = 404;
    allXorms = [];
    success = false;
  }

  return res.status(status).json({
    success: success,
    data: allXorms
  });
}

exports.save = async (req, res) => {

  let status = 200,
      success = true;
  let body = req.body;
  debug('xorm CREATE');
  debug(body);

  let xorm = new Xorm(body);
  xorm.author = req.user._id;
  const collaborator = {
    id: req.user._id,
    name: req.user.fullName,
    email: req.user.email,
    role: 'owner',
    invitationDate: Date(),
    answerDate: Date(),
    status: 'accepted'
  }
  xorm.settings.collaborators.push(collaborator);
    
  try {
    let result = xorm.save();
  } catch (e) {
    debug("Error when saving XORM");
    debug(e);
    xorm = undefined;

    status = 400;
    success = false;
  }

  return res.status(status).json({
    success: success,
    xorm:  xorm
  })
}

exports.update = async (req, res) => {

  let status = 200,
      success = true;
  let body = req.body;
  debug('xorm CREATE');
  debug(body);

  const { xormId } = req.params;
  
  try {
    await Xorm.findByIdAndUpdate(xormId, body);

  } catch (e) {
    debug("Error when saving XORM");
    debug(e);
    // xorm = undefined;

    status = 400;
    success = false;
  }

  return res.status(status).json({
    success: success,
    // xorm:  xorm
  })
}

exports.fetchOne = async function(req, res) {
  var xormId = req.params.xormId;
  var xormOne = {};
  var success = true;

  try {
    xormOne = await Xorm.findById(xormId).populate('forms');
    console.log(xormOne);
  } catch (e) {
    debug("Error when fetch one xorm - " + xormId);
    debug(e);

    xormOne = {}
    success = false;
  }

  return res.json({
    success: success,
    data: xormOne
  });
}

exports.download = async function(req, res) {
  let xormId = req.params.xormId;
  let xormGenerated;

  Xorm.findById(xormId).populate('forms').exec((err, xormOne) => {
    if (err) {
      return res.status(204).json({
        success: false
      });
    }

    xormGenerated = {
      _id: xormOne._id,
      name: xormOne.title,
      description: xormOne.description,
      xorms: [],
      xormsDetails: {}
    };

    for (const form of xormOne.forms) {
      xormGenerated.xorms.push({
        id: form._id,
        title: form.title
      });

      xormGenerated.xormsDetails[form._id] = {
        ...form.form
      }
    }

    return res.json({
      success: true,
      data: xormGenerated
    });

  });
}

exports.generate = (req, res) => {
  let xormId = req.params.xormId;
  let xormGenerated;

  debug('\nGENERATE...');
  Xorm.findById(xormId).populate('forms').exec((err, xormOne) => {
    if (err) {
      return res.status(204).json({
        success: false
      });
    }

    xormGenerated = {
      _id: xormOne._id,
      name: xormOne.title,
      description: xormOne.description,
      xorms: [],
      xormsDetails: {}
    };

    for (const form of xormOne.forms) {
      xormGenerated.xorms.push({
        id: form._id,
        title: form.title
      });

      xormGenerated.xormsDetails[form._id] = {
        ...form.form
      }
    }

    // debug("\nXorm TREATED...");
    // debug(xormGenerated);
    const filename = `${xormId}_${Date.now()}.json`;
    fs.writeFile(`public/${xormId}_${Date.now()}.json`, JSON.stringify(xormGenerated), 'utf8', (err) => {
      if (err) throw err;
      debug("\nFS WRITE FILE.. "); // , path.resolve(`public/${xormId}_${Date.now()}.json`));

      // res.download(path.resolve(`public/${filename}`), filename, (err) => {
      //   if (err) {
      //     // throw err;
          
      //     debug("\nERRORRR.... ", err);
      //   }
      //   debug("\nNO ERROR... ");
      // });
      const file = fs.createReadStream(path.resolve(`public/${filename}`));
      res.writeHead(200, {'Content-disposition': `attachment; filename=${filename}`}); 
      file.pipe(res);
    });

    // var files = fs.createReadStream("demo.pdf");
    // res.writeHead(200, {'Content-disposition': 'attachment; filename=demo.pdf'}); //here you can add more headers
    // files.pipe(res)

    // return res.json({
    //   status: true,
    //   xorm: xormGenerated
    // })
    
  });
}
