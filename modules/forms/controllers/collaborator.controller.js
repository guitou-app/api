const mongoose = require('mongoose');
const debug = require('debug')('guitou:collaborators.controller');
const Xorm = mongoose.model('Xorm');
const User = mongoose.model('User');
// const { publishToQueue } = require('../helpers/rabbitmq');
const { publishToQueue } = require('../../../config/lib/rabbitmq');

exports.fetchAll = async function(req, res) {
  var allCollaborators = [];
  var success = true;
  var status = 202;

  debug("CollaboratorS ... fecthAll() --- ", req.params.xormId);
  debug(req.user);

  try {
    // allCollaborators = await Collaborator.find({ author: req.user._id, xorm: req.params.xormId });
    // allCollaborators = await Collaborator.find({ xorm: req.params.xormId });
    const xormOne = await Xorm.findById(req.params.xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);
    allCollaborators = xormOne.settings.collaborators.filter(f => f.status != 'rejected');

  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);

    status = 404;
    allCollaborators = [];
    success = false;
  }

  return res.status(status).json(allCollaborators);
}

exports.invite = async function(req, res) {
  const { xormId } = req.params;
  const { email, role } = req.body;

  let success = true;
  let status = 202;
  let collaborator;


  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    
    collaborator = xormOne.settings.collaborators.find(c => c.email == email);
    if (!collaborator) {
      // check if user exists
      collaborator = {
        email,
        role,
        status: 'waiting'
        // invitationDate: Date()
      };
      
      xormOne.settings.collaborators.push(collaborator);
      await xormOne.save();
    }

    collaborator = xormOne.settings.collaborators.find(c => c.email == email);
    
    // Send through RabbitMQ for Emailing
    const data = {
      projectId: xormId,
      projectName: xormOne.title,
      email,
      invitationId: collaborator._id
    };
    debug("PUBLISHING.... ");
    debug(data);
    publishToQueue('guitou.project.user.invitated', JSON.stringify(data));
  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);

    status = 404;
    allCollaborators = [];
    success = false;
  }

  return res.status(status).json(collaborator);
}

exports.remove = async function (req, res) {
  const { xormId, invitationId } = req.params;

  try {
    const xormOne = await Xorm.findById(xormId);
    debug('XORM ONE');
    debug(xormOne);

    collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId); // c.email == email);
    if (!collaborator) {
      throw new Error('Sorry but this collaborator does not exists');
    }

    await Xorm.findOneAndUpdate(
      {
        _id: xormId,
        "settings.collaborators._id": invitationId
      },
      {
        $pull: {
          "settings.collaborators": {
            "_id": invitationId
          }
        }
      }
    )

  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);
  }

  return res.status(201).json({});
}

exports.join = async function(req, res) {
  const { xormId, invitationId } = req.params;
  const { email, answer } = req.body;
  let success = true;
  let status = 202;
  let collaborator;

  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId); // c.email == email);
    if (!collaborator) {
      throw new Error('not invitation to join this project for this email user')
    }

    const user = await User.findOne({ email: collaborator.email });
    if (user) {
      collaborator['name'] = user.name;
      collaborator['userId'] = user.id;
      collaborator['status'] = answer == 'yes' ? 'accepted' : 'rejected';
      collaborator['answerDate'] = Date();

      debug('New Collaborator');
      debug(collaborator);
    } else {
      throw new Error('you need to first create an account');
    }

    await Xorm.findOneAndUpdate(
      {
        _id: xormId,
        "settings.collaborators._id": invitationId
      },
      {
        // $set: {
          "$set": {
            "settings.collaborators.$": collaborator
          }
        // }
      }
    )

  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);

    status = 404;
    allCollaborators = [];
    success = false;
  }

  return res.status(status).json(collaborator);
}

exports.check = async function(req, res) {
  const { xormId, invitationId } = req.params;
  let xormOne;
  let collaborator;

  try {
    xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId);
    if (!collaborator) {
      throw new Error('Sorry but that invitation does not exists');
    }

    if (collaborator.status !== 'waiting') {
      throw new Error("Error! It's no longer an invitation!");
    }
  } catch(e) {
    throw new Error('Error! An error occured during the check!');
  }

  return res.status(201).json({
    project: {
      title: xormOne.title,
      description: xormOne.description
    },
    collaborator,
    email: collaborator.email
  });
}

exports.cancel = async function(req, res) {
  const { xormId, invitationId } = req.params;
  let invitation;

  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    const collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId);
    if (!collaborator) {
      throw new Error('Sorry but that invitation does not exists');
    }

    if (collaborator.status !== 'waiting') {
      throw new Error("Error! It's no longer an invitation! Not possible to cancel!");
    }

    await Xorm.findByIdAndUpdate(xormId, {
      $pull: { 
        "settings.collaborators": { 
          _id: invitationId
        } 
      }
    });

    invitation = collaborator;
  } catch(e) {
    throw new Error('Error! An error occured during the canceling');
  }

  return res.status(201).json(invitation);
}