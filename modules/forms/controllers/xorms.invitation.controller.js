const mongoose = require('mongoose');
const debug = require('debug')('guitou:collaborators.controller');
const Xorm = mongoose.model('Xorm');

exports.fetchAll = async function(req, res) {
  var allCollaborators = [];
  var success = true;
  var status = 202;

  debug("CollaboratorS ... fecthAll() --- ", req.params.xormId);
  debug(req.user);

  try {
    // allCollaborators = await Collaborator.find({ author: req.user._id, xorm: req.params.xormId });
    // allCollaborators = await Collaborator.find({ xorm: req.params.xormId });
    const xormOne = await Xorm.findById(req.params.xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);
    allCollaborators = xormOne.settings.collaborators.filter(f => f.status == 'accepted');

  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);

    status = 404;
    allCollaborators = [];
    success = false;
  }

  return res.status(status).json(allCollaborators);
}

exports.invite = async function(req, res) {
  const { xormId } = req.params;
  const { email, role } = req.body;

  let success = true;
  let status = 202;

  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    // check if user exists
    const collaborator = {
      email,
      role,
      status: 'waiting'
      // invitationDate: Date()
    };
    xormOne.settings.collaborators.push(collaborator);
    await xormOne.save();

    // Send through RabbitMQ for Emailing
  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);

    status = 404;
    allCollaborators = [];
    success = false;
  }

  return res.status(status).json();
}

exports.join = async function(req, res) {
  const { xormId, invitationId } = req.params;
  const { email, answer } = req.body;
  let success = true;
  let status = 202;

  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    const collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId); // c.email == email);
    if (!collaborator) {
      // return res.status(402).json({
      //   success: false,
      //   message: 'not invitation to join this project for this email user'
      // });
      throw new Error('not invitation to join this project for this email user')
    }

    const user = await User.findOne({ email: c.email });
    if (user) {
      collaborator['name'] = user.name;
      collaborator['userId'] = user.id;
      collaborator['status'] = answer == 'yes' ? 'accepted' : 'rejected';
      collaborator['answerDate'] = Date();
    } else {
      // return res.status(402).json({
      //   success: false,
      //   message: 'you need to first create an account'
      // });
      throw new Error('you need to first create an account');
    }

    await Xorm.findOneAndUpdate(
      {
        _id: xormId,
        "settings.collaborators._id": invitationId
      },
      {
        $set: {
          "$set": {
            "settings.collaborators.$": collaborator
          }
        }
      }
    )

  } catch(e) {
    debug("Error when fetching Collaborators....");
    debug(e);

    status = 404;
    allCollaborators = [];
    success = false;
  }

  return res.status(status).json({
    success: success
  });
}

exports.check = async function(req, res) {
  const { xormId, invitationId } = req.params;
  
  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    const collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId);
    if (!collaborator) {
      throw new Error('Sorry but that invitation does not exists');
    }

    if (collaborator.status !== 'waiting') {
      throw new Error("Error! It's no longer an invitation!");
    }
  } catch(e) {
    throw new Error('Error! An error occured during the check!');
  }

  return res.status(201).json();
}

exports.cancel = async function(req, res) {
  const { xormId, invitationId } = req.params;
  let invitation;

  try {
    const xormOne = await Xorm.findById(xormId); // .populate('Collaborators');
    debug('XORM ONE');
    debug(xormOne);

    const collaborator = xormOne.settings.collaborators.find(c => c._id == invitationId);
    if (!collaborator) {
      throw new Error('Sorry but that invitation does not exists');
    }

    if (collaborator.status !== 'waiting') {
      throw new Error("Error! It's no longer an invitation! Not possible to cancel!");
    }

    await Xorm.findByIdAndUpdate(xormId, {
      $pull: { 
        "settings.collaborators": { 
          _id: invitationId
        } 
      }
    });

    invitation = collaborator;
  } catch(e) {
    throw new Error('Error! An error occured during the canceling');
  }

  return res.status(201).json(invitation);
}