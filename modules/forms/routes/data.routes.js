'use strict';

module.exports = (app) => {

  let data = require('../controllers/data.controller');

  app.route('/api/forms/:formId/data')
    .get(data.getForm)
   .post(data.save);

  app.route('/api/m/forms/:formId/data')
   .post(data.save)
   .get(data.fetchAll);

  app.route('/api/a/xorms/:xormId/data')
   .post(data.save)
   .get(data.fetchAll);

  app.route('/api/a/xorms/:xormId/data/:dataId')
   .get(data.fetch)
   .delete(data.delete)
   .put(data.update);
   
}
