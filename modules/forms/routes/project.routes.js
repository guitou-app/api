'use strict';

module.exports = (app) => {
  let xorms = require('../controllers/xorms.controller');

  app.route('/api/xorms/:xormId/download')
    .get(xorms.download)
  ;

  app.route('/api/a/xorms/:xormId')
    .get(xorms.fetchOne)
    .put(xorms.update);

  app.route('/api/a/xorms')
    .get(xorms.fetchAll)
    .post(xorms.save);

  app.route('/api/xorms/:xormId/generate')
    .get(xorms.generate)
    ;
}
