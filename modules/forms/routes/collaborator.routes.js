'use strict';

module.exports = (app) => {
  let collaborator = require('../controllers/collaborator.controller');
  
  app.route("/api/a/xorms/:xormId/collaborators")
    .get(collaborator.fetchAll)
    // .post(collaborator.save)
    ;
  
  app.route("/api/a/xorms/:xormId/invitation/:invitationId")
    .delete(collaborator.remove)
  ;
  
  app.route("/api/a/xorms/:xormId/invitation")
    .post(collaborator.invite)
    ;
  
  app.route("/api/a/xorms/:xormId/invitation/:invitationId/check")
    .post(collaborator.check)
    ;

  app.route("/api/a/xorms/:xormId/invitation/:invitationId/answer")
    .post(collaborator.join)
    ;
    
}
