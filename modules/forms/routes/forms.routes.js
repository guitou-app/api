'use strict';

module.exports = (app) => {
  let forms = require('../controllers/forms.controller');

  app.route("/api/test")
    .get((req, res) => {
      return res.status(200).json({
        message: "It's running"
      });
    });
  
  app.route("/api/a/xorms/:xormId/forms")
    .get(forms.fetchAll)
    .post(forms.save);

  app.route('/api/a/xorms/:xormId/forms/:formId')
    .get(forms.fetchOne)
    // .put(forms.update)
  ;
  app.route('/api/forms/:formId/download')
    .get(forms.download)
    // .put(forms.update)
  ;

  app.route('/api/a/forms')
    .get(forms.fetchAll)
    .post(forms.save)
  ;

  app.route('/api/a/forms/:formId')
    .get(forms.fetchOne)
    .put(forms.update)
  ;

  app.route('/api/a/forms/:formId/collaborators')
    .get(forms.fetchAllCollaborators)
    .post(forms.addCollaborator)
  ;

  app.route('/api/a/forms/:formId/collaborators/:collaboratorId')
    // .get(forms.fetchAllCollaborators)
    .delete(forms.removeCollaborator)
  ;

  app.route('/api/forms/:formId/collaborators/auth')
    .post(forms.collaboratorAuth)
  ;

  app.route('/api/forms/:formId/generate')
    .get(forms.generateMobileApp)
  ;
}
