'use strict';

module.exports = (app) => {
  let stats = require('../controllers/stats.controller');

  app.route('/api/a/xorms/:xormId/dashboards')
    .get(stats.fetchOne)
    .post(stats.addChart)
  ;

  app.route('/api/a/xorms/:xormId/dashboards/init')
    .post(stats.initChart)
  ;

  app.route('/api/a/xorms/:xormId/dashboards/:chartId')
    .delete(stats.deleteChart)
  ;


}
