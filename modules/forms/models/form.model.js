'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var FormSchema = Schema({
  title: {
    type: String,
    default: "Untitled form"
  },
  description: {
    type: String
  },
  form: mongoose.Mixed,
  // form: {}
  settings: {
    display_data: {
      web: {
        columns: [mongoose.Mixed],
        data: []
      },
      mobile: {
        template: String,
        fields: mongoose.Mixed
      }
    }
  },
  data: [mongoose.Mixed],
  collaborators: [{
    name: String,
    email: String,
    username: String,
    password: String,
    phone: String,
    role: String,
    salt: String
  }],

  author: {
    type: String,
    ref: 'User'
  },
  xorm: {
    type: String,
    ref: 'Xorm'
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
}, { strict: false });

exports.FormSchema = FormSchema;

mongoose.model('Form', FormSchema);
