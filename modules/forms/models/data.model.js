'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


let DataSchema = mongoose.Schema({

  /**
   * Properties to handle data coming from Mobile
   */
  // mobileId: Number,
  // _remote_id: String,
  // _local_id: String,
  // savedRemotelyAt: {
  //   type: Date,
  //   default: Date.now
  // },

  
  primaryKey: {
    type: Schema.Types.Mixed,
    required: true
  },

  /**
   * Where does the data comes from ?
   * mobile: a mobile user
   * browser: a guest
   * remote: a admin
   */
  origin: {
    type: String,
    enum: [
      'web', // webapp - angular
      'mobile', // mobile - flutter
      'guest' // www - reactjs
    ]
  },
  /**
   * The user location
   */
  location: {
    ip: String,
    coords: {
      longitude: String,
      latitude: String,
      altitude: String
    }
  },
  oldValues: [{
    createdAt: {
      type: Date,
      default: Date.now
    },
    values: mongoose.Mixed
  }],

  /**
   * The data from the questionnaire
   */
  values: mongoose.Mixed,

  /**
   * The form
   */
  form: {
    type: String,
    ref: 'Form'
  },

  createdAt: {
    type: Date,
    default: Date.now
  },
}, { strict: false })

// DataSchema.options.toJSON = {
//   transform: function(doc, ret) {
//     ret._remote_id = ret._id;
//     delete ret._id;
//   }
// };

mongoose.model('Data', DataSchema);
