var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let DashboardSchema = mongoose.Schema({
  form: {
    type: String,
    ref: 'Form'
  },
  xorm: {
    type: String,
    ref: 'Xorm'
  },
  charts:[{
    form: String,
    name: String,
    variable: String,
    question: Object,
    ratas: []
  }],
  createdAt: {
    type: Date,
    default: Date.now
  }
}, { strict: false });

mongoose.model('Dashboard', DashboardSchema)
