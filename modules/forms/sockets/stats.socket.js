var fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose'),
    debug = require('debug')('dashboard.socket')
;
const { ObjectId } = mongoose.Types;
const Form = mongoose.model('Form');
const Data = mongoose.model('Data');
const fsUtil = require('../helpers/fs.util');

const assert = require('assert');


module.exports = function(io, socket) {
  socket.on('dashboard.create.chart.data', async (params, callback) => {
    debug('dashboard.create.chart.data');
    debug(params);
    const { formId, question, chart } = params;

    switch (chart) {
      case "bar":
        debug("SW bar");
        DataAggregation.bar(formId, question, callback);
        break;

      case "histogram":
        DataAggregation.histogram(formId, question, callback);
        break;

      case "boxplot":
        DataAggregation.boxplot(formId, question, callback);
        break;

      case "pie":
        DataAggregation.pie(formId, question, callback);
        break;

      default:
        debug("SW DF " + chart);
    }
  });
}

let DataAggregation = {
  split(question) {
    debug("SPKIT ... " + question);
    return {
      section: String(question).split("__")[0],
      varid: question
    };
  },
  async histogram(formId, question, callback) {
    let { section, varid } = this.split(question);
    
    Data.aggregate([
      { $match: {
          "form": formId
        }
      },
      { $project: {
          "variable": {
            $convert: {
              input: `$values.${section}.${varid}`,
              to: "int",
              onError: 0
            }
          }
        }
      },
      { $group: {
          "_id": null,
          "stats": { $push: "$variable" }
        }
      }
    ]).exec(function(err, results) {
      assert.equal(err, null);
      debug("\nHistogram-----------------");
      console.log(err);
      console.log(results);

      callback((results.length > 0 ? results[0].stats : []));
    });
  },
  boxplot(formId, question, callback) {
    this.histogram(formId, question, callback);
  },
  bar(formId, question, callback) {
    let { section, varid } = this.split(question);
    debug("FEDDD BAR ", formId, new ObjectId(formId));
    
    const agg = [
      {
        '$match': {
          '_id': new ObjectId(formId)
        }
      }, {
        '$project': {
          'map': {
            '$objectToArray': `$form.${section}.questions.${varid}.kv`
          }, 
          '_id': 0
        }
      }, {
        '$unwind': {
          'path': '$map', 
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$project': {
          'k': '$map.k', 
          'name': '$map.v',
          'value': { $literal: 0}
        }
      }, {
        '$addFields': {
          'value': 0
        }
      }, {
        '$lookup': {
          'from': 'datas', 
          'let': {
            'key': '$name'
          }, 
          'pipeline': [
            {
              '$match': {
                'form': formId
              }
            }, {
              '$project': {
                'variable': `$values.${section}.${varid}`, 
                '_id': 0
              }
            }, {
              '$group': {
                '_id': '$variable', 
                'value': {
                  '$sum': 1
                }
              }
            }, {
              '$project': {
                'k': '$_id', 
                'value': 1, 
                '_id': 0
              }
            }, {
              '$match': {
                '$expr': {
                  '$eq': [
                    '$$key', '$k'
                  ]
                }
              }
            }
          ], 
          'as': 'stats'
        }
      }, {
        '$replaceRoot': {
          'newRoot': {
            '$mergeObjects': [
              '$$ROOT', {
                '$arrayElemAt': [
                  '$stats', 0
                ]
              }
            ]
          }
        }
      }, {
        '$project': {
          'stats': 0
        }
      }
    ];
    Form.aggregate(agg).exec(function(err, results) {
      assert.equal(err, null);
      debug("\nBAR-----------------");
      debug(err);
      debug(results);

      results = results.map((d) => {
        return {
          index: d.k,
          key: d.name,
          value: d.value
        }
      }).sort((a, b) => {
        return +a.index > +b.index;
      });

      debug("\nAFTER MAP & SORTING");
      debug(results);

      callback(results);
    });
  },
  pie(formId, question, callback) {
    this.bar(formId, question, callback)
  }
}
