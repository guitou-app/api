var fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose'),
    debug = require('debug')('forms.socket')
;

const { waterfall } = require('async');
const { exec, execFile, spawn } = require('child_process');
const Form = mongoose.model('Form');
const Xorm = mongoose.model('Xorm');
const fsUtil = require('../helpers/fs.util');

module.exports = function(io, socket) {

  socket.on('forms.new', async function(params, callback) {
    debug("forms.new");
    debug(params);
    var success = true;
    var newForm = new Form({
      title: "Untitle Form",
      description: "",
      form: {
        "section_0": {
          _params: { title: "Untitled Section", description: "", key: "section_0" },
          questions: []
        }
      },
      xorm: params.xormId
    });
    debug(newForm);

    try {
      let result = await newForm.save();
      debug(result);
      callback({
        form: newForm._id,
        success: success
      });
      let xormOne = await Xorm.findById(params.xormId);
      xormOne.forms = [...xormOne.forms, newForm._id];
      if (!xormOne.settings.forms.primary) {
        xormOne.settings.forms.primary = newForm._id
      }
      await xormOne.save();

    } catch(e) {
      // {
      debug("Error when creating forms....");
      debug(e);

      success = false;
      callback({
        form: newForm,
        success: success
      });
    }
  });

  socket.on('forms.save.auto', async function(params, callback) {
    debug("forms.save.auto");
    debug(params);

    Form.findOneAndUpdate({
      "_id": params.id
    }, {
      "$set": {
        "title": params.form.title,
        "form": params.form.form
      }
    }, function(err, form) {
      if (err) {
        callback({
          success: false,
          result: "Errors..."
        });
      } else {
        callback({
          success: true,
          result: "Successful..."
        });
      }
    });
  })

  socket.on('forms.generate', async function (params, callback) {
    var formId = params.formId;
    var theCurrentForm = undefined;
    var status = undefined;
    var success = true;
    var error = undefined;
    const baseDir = '../Customers/generated';

    debug("forms.generate");
    debug(params);
    // TODO: Fetch form information from DB
    try {
      theCurrentForm = await Form.findById(formId);
    } catch (e) {
      debug("Error when fetching form information");
      debug(e);

      error = e;
      success = false;
      status = 404;
    }

    if (typeof(theCurrentForm) == 'undefined') {
      return res.status(status).json({
        success: success,
        error: error
      })
    }

    debug("THE CURRENT FORM");
    debug(theCurrentForm);

    debug("\nDIRNAME.... ", __dirname);
    var appName = theCurrentForm._id + '_' + new Date().getTime(); // .split(" ").join("");
    var appPath = path.join(__dirname,'../../../../Generated', appName);
    var appTemplatePath = path.join(__dirname, '../../../../Mobile');
    debug(appPath);

    // TODO: Check if a folder with the form Title exists, if so delete it

    socket.emit("forms.generate.messages", {
      step: `Checking if ${appName} folder already exists.`
    });
    if (fs.existsSync(appPath)) {
      socket.emit("forms.generate.messages", {
        type: "success",
        message: `${appName} folder exist`
      });

      // TODO: Delete the folder
      fsUtil.rmdir(appPath);
      debug("DELETE ... ", appPath);
      socket.emit("forms.generate.messages", {
        type: "success",
        message: `${appName} folder deleted`
      });
    } else {
      socket.emit("forms.generate.messages", {
        type: "success",
        message: `${appName} folder doesn't exist`
      });
    }

    // TODO: Create the App folder
    socket.emit("forms.generate.messages", {
      step: `Creating ${appName} folder.`
    });
    fs.mkdirSync(appPath); //, 0755);
    debug("Mkdir ... ", appPath);
    socket.emit("forms.generate.messages", {
      type: "success",
      message: `${appName} folder successfully created.`
    });

    // TODO: Copy the BoilerPlate folder : into a new folder
    socket.emit("forms.generate.messages", {
      step: `Creating ${appName} application.`
    });
    fsUtil.copyDir(appTemplatePath, appPath);
    debug("Copied ... ", appTemplatePath, appPath);
    socket.emit("forms.generate.messages", {
      type: "success",
      message: `${appName} application successfully created.`
    });

    // TODO: Update the form.json
    fs.writeFileSync(path.join(appPath, 'src/Helpers/forms.json'), JSON.stringify(theCurrentForm, null, 2));
    debug("Update the form.json");

    // TODO: Update the application name
    var appJsonContent = {
      "name": appName,
      "displayName": theCurrentForm.title
    }
    fs.writeFileSync(path.join(appPath, 'app.json'), JSON.stringify(appJsonContent, null, 2));
    debug("Update the app.json");

    // TODO: Update package.json. 1- Read all the file; 2- Change "name" and "version" to the appName and formVersion; 3- Write it back

    // TODO: Generate the APK release: All the console must be save into the logs folders. The file name : {username}_generate_apk_{date}.log
    debug(process.env.PATH);
    // var env = getEnvKeyValuePairsSomeHow();

    waterfall([
      // TODO: Set the working Directory cLI to appPath
      // function(done) {
      //   debug("CD .. ", appPath);
      //
      //   // execFile('cd ' + appPath, [], (error, stdout, stderr) => {
      //   execFile('pwd', [], (error, stdout, stderr) => {
      //     if (error) {
      //       // debug(error);
      //       throw error;
      //       done(error);
      //     }
      //     debug(stdout);
      //     debug(stderr);
      //
      //     done(null)
      //   })
      // },

      // TODO: Run the CLI command to install packagees: npm install (Think of copying the project with the node_modules folder)
      // function(done) {
      //
      // }

      // TODO: Run the CLI Command to generate the APK: cd android && ./gradlew assembleRelease
      function(done) {
        // debug('CD android...');
        debug('CHMOD gradlew...');

        // execFile('cd', ['android'], (error, stdout, stderr) => {
        //   if (error) {
        //     throw error
        //     done(error);
        //   }
        //   debug(stdout);
        //   debug(stderr);
        //
        //   done(null)
        // })
        fs.chmod(path.join(appPath, 'android/gradlew'), 0o755, (error) => {
          if (error) {
            throw error;
            done(error);
          }

          debug("CHMOD gradlew finished........**___****____");
          done(null);
        })
      },

      function(done) {
        debug("RUN ./gradlew assembleRelease", path.join(appPath, 'android'));

        const gradlew = spawn('bash', ['gradlew', 'assembleRelease'], { cwd: path.join(appPath, 'android') });

        var errorparams = "";
        gradlew.stdout.on('params', (params) => {
          debug(`stdout: ${params}`);

          socket.emit("forms.generate.messages", {
            step: "runing-gradlew",
            type: "output",
            message: `${params}`
          });
        });

        gradlew.stderr.on('params', (params) => {
          debug(`stderr: ${params}`);
          errorparams += params;

          socket.emit("forms.generate.messages", {
            step: "runing-gradlew",
            type: "error",
            message: `${params}`
          });
        });

        gradlew.on('close', (code) => {
          debug(`\nClose process exited with code ${code}`);

          socket.emit("forms.generate.messages", {
            step: "runing-gradlew",
            type: "error",
            message: `Close process exited with code ${code}`
          });

          if (code == 1) {
            done(errorparams);
          } else {
            done(null);
          }
        });
        // execFile('bash', ['gradlew', 'assembleRelease'], { cwd: path.join(appPath, 'android') }, (error, stdout, stderr) => {
        //   debug(stdout);
        //   debug(stderr);
        //   if (error) {
        //     throw error
        //     done(error);
        //   }
        //
        //   done(null)
        // })
      },

      // TODO: CLI Command: Rename the apk file : FormTitle + CurrentDate
      // TODO: CLI Command: Move the APK File to the download directory
      function(done) {
        debug("CP APK File...........");

        var apkName = appName + '_' + Date.now;
        execFile('cp', ['app/build/outputs/apk/release/apk-release', './' + apkName], (error, stdout, stderr) => {
          if (error) {
            throw error
            done(error);
          }
          debug(stdout);
          debug(stderr);

          done(null);
          callback(true);
        })
      }


    ], function(err, result) {
      debug("AAASSSYYYNNNNNNNCC ---- ERR RESULT");
      debug(err);
      debug(result);
      callback([err, result]);
    });

    // TODO: All the result

    // TODO: Send the APK back to the user or a link to download it
  });

  socket.on('forms.dashboard.join', function(params, callback) {
    debug('forms.dashboard.join');
    debug(params);

    socket.join(`forms.${params.formId}.dashboard`);
    callback(true);
  });
}
