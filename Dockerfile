FROM node:12 
# -alpine

# RUN apt-get update && apt-get install -y \
#         iputils-ping \
#         net-tools \
#     && rm -rf /var/lib/apt/lists/*

WORKDIR /src

ADD wait-for-it.sh wait-for-it.sh
RUN chmod +x /src/wait-for-it.sh


ADD package*.json /src/
RUN npm install --silent

# COPY . .
ADD . /src

# ENV NODE_ENV 'development'
# ENV RABBITMQ_USER 'guitou' 
# ENV RABBITMQ_USER_PWD 'guitou-dev' 
# ENV RABBITMQ_VHOST 'guitou'

EXPOSE 3000

CMD npm run prod

# ENTRYPOINT [ "/bin/bash" ]
# CMD ["/usr/src/guitou/api/wait-for-it.sh", "${RABBITMQ_TCP_ADDR}:5672", "--strict", "--timeout=1000" , "--", "node", "app.js"]

# ENTRYPOINT [ "/bin/bash", "-c" ]
# CMD ["node", "app.js"]
# ENTRYPOINT ["node", "app.js"]
