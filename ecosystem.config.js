module.exports = {
  apps : [
    // {
    //   name: 'guitou-api',
    //   script: './app.js',

    //   // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    //   // args: 'one two',
    //   instances: 1,
    //   autorestart: true,
    //   watch: true,
    //   max_memory_restart: '1G',
    //   env: {
    //     NODE_ENV: 'development'
    //   },
    //   // env_production: {
    //   //   NODE_ENV: 'production',
    //   //   RABBITMQ_URL: 'amqp://fytarnlq:DK-2lzClRLQnSTFOgdM_H2A6kwucP0J_@spider.rmq.cloudamqp.com/fytarnlq',
    //   //   REDIS_URL: 'redis://h:p0c6834b5df2bdb329f1c6484da0f9a341d112e0841e3a5330fec5e401c8841fc@ec2-52-70-49-106.compute-1.amazonaws.com:7169'
    //   // },
    //   env_test: {
    //     NODE_ENV: 'test'
    //   }
    // },
    {
      name: 'guitou-data-analysis-consumer(add)',
      script: './scripts/data-analysis-add.consumer.js',
      instances: 1,
      autorestart: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        MONGODB_URI: 'mongodb://localhost:27020',
        RABBITMQ_URL: 'amqp://guitou:guitou-dev@localhost/guitou' // 'http://192.168.1.100:15672';
      },
      env_production: {
        NODE_ENV: 'production',
        RABBITMQ_URL: 'amqp://guitou:guitou-dev@192.168.1.100/guitou-dev'
      }
    }
  ],

  // deploy : {
  //   production : {
  //     user : 'node',
  //     host : '212.83.163.1',
  //     ref  : 'origin/master',
  //     repo : 'git@github.com:repo.git',
  //     path : '/var/www/production',
  //     'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
  //   }
  // }
};
