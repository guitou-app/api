'use strict';

var gulp = require('gulp')
    , nodemon = require('gulp-nodemon')
    , gulpLoadPlugins = require('gulp-load-plugins'),
    plugins = gulpLoadPlugins({
      rename: {
        'gulp-angular-templatecache': 'templateCache'
      }
    });
var _ = require('lodash');
var runSequence = require('gulp4-run-sequence'); // require('run-sequence')

const REMOTE_SERVER = '192.168.8.100';
const RABBITMQ_USER = 'guitou';
const RABBITMQ_USER_PWD = 'guitou-dev';
const RABBITMQ_VHOST = 'guitou';

// Nodemon task
gulp.task('nodemon:app', function () {
  // return plugins.nodemon({
  return nodemon({
    script: 'app.js',
    verbose: true
  });
});

gulp.task('nodemon:danalysis-add-consumer', function () {
  return plugins.nodemon({
    script: 'scripts/data-analysis-add.consumer.js',
    verbose: true
  });
});

// Set NODE_ENV to 'development'
gulp.task('env:dev', function (done) {
  process.env.DEBUG = '*';
	// process.env.NODE_ENV = 'development';

  // process.env.RABBITMQ_TCP_ADDR = REMOTE_SERVER;
  // process.env.RABBITMQ_USER = RABBITMQ_USER;
  // process.env.RABBITMQ_USER_PWD = RABBITMQ_USER_PWD;
  // process.env.RABBITMQ_VHOST = RABBITMQ_VHOST;

  // process.env.REDIS_HOST = REMOTE_SERVER;

  // process.env.DB_1_PORT_27017_TCP_ADDR = REMOTE_SERVER;
  
  done();
  // process.env.RABBITMQ_URL = 'amqp://guitou:guitou-dev@192.168.1.104/guitou-dev'; // 'http://192.168.1.100:15672';
});

gulp.task('danalysis-add-consumer', function (done) {
    runSequence('env:dev', ['nodemon:danalysis-add-consumer'], done);
});

// Run the project in development mode with node debugger enabled
gulp.task('default', function (done) {
    runSequence('env:dev', ['nodemon:app'], done);
    // runSequence('env:dev', ['nodemon:app','nodemon:danalysis-add-consumer'], done);
    // done();
});
