'use strict';

module.exports = {
  db: {
    uri: 'mongodb+srv://guitou-api:dMADTZOoWE7cbR7w@guit01-kvpbr.mongodb.net/test?retryWrites=true&w=majority', // process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb+srv://guitou:@dm1nGuIt0u@cluster0-fikfb.mongodb.net/test?retryWrites=true&w=majority',
    options: {
      useNewUrlParser: true
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || true,
    username: 'guitou',
    password: '@dm1nGuIt0u'
  },
  rabbitmq: {
    uri: process.env.RABBITMQ_URL || process.env.RABBITMQ_URI || 'amqp://' + (process.env.RABBITMQ_TCP_ADDR || 'localhost')
  },
  redis: {
    url: 'redis://h:p0c6834b5df2bdb329f1c6484da0f9a341d112e0841e3a5330fec5e401c8841fc@ec2-54-152-118-90.compute-1.amazonaws.com:13179',
  	host: process.env.REDIS_HOST || 'ec2-54-152-118-90.compute-1.amazonaws.com',
  	port: process.env.REDIS_PORT || 13179,
    user: 'h',
    password: 'p0c6834b5df2bdb329f1c6484da0f9a341d112e0841e3a5330fec5e401c8841fc',
  	options: {}
  },
}
