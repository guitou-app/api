'use strict';

module.exports = {
  app: {
    title: 'Guitou',
    description: '',
    keywords: ''
  },

  redis: {
  	host: process.env.REDIS_HOST || '127.0.0.1',
  	port: process.env.REDIS_PORT || 6379,
  	options: {}
  },

  db: {
    promise: global.Promise,
    debug: true
  },

  jwt: {
    secret: 'Guitou',
    expiration: 1000 * 60 * 60
  },

  port: process.env.PORT || 3000,
  host: process.env.HOST || '0.0.0.0',

  domain: process.env.DOMAIN,

  rabbitmq: {
    debug: process.env.RABBITMQ_DEBUG || true,

    queues: [
      'guitou.data.added',
      'guitou.data.updated',
      'guitou.data.removed'
    ],
    queue: {
      add: 'guitou.data.added',
      update: 'guitou.data.updated',
      remove: 'guitou.data.removed'
    }
  },
}
