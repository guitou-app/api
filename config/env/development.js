'use strict';

module.exports = {
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || '192.168.8.102') + '/guitou-dev',
    options: {
      useNewUrlParser: true
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || true,
    dbname: 'guitou-dev'
  },
  rabbitmq: {
    uri: process.env.RABBITMQ_URL || process.env.RABBITMQ_URI || 'amqp://' + process.env.RABBITMQ_USER + ':' + process.env.RABBITMQ_USER_PWD + '@' + (process.env.RABBITMQ_TCP_ADDR || '192.168.8.101') + '/' + process.env.RABBITMQ_VHOST,
  },
  redis: {
    url: 'redis://' + process.env.REDIS_HOST + ':6379',
    host: 'redis://' + (process.env.REDIS_HOST || '192.168.8.101'),
    port: process.env.REDIS_PORT || 6379,
    options: {}
  },
}
