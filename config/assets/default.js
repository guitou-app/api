'use strict';

module.exports = {
    gulpConfig: ['gulpfile.js'],
    allJS: ['server.js', 'config/**/*.js', 'modules/**/*.js'],
    models: 'modules/*/models/**/*.js',
    routes: ['modules/*/routes/**/*.js'],
    sockets: 'modules/*/sockets/**/*.js',
    config: ['modules/*/config/*.js'],
    policies: 'modules/*/policies/*.js',
    views: ['modules/*/views/*.html']
};
