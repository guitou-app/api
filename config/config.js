'use strict';

/**
 * Dependencies
 */
var _ = require('lodash'),
    chalk = require('chalk'),
    glob = require('glob'),
    fs = require('fs'),
    path = require('path'),
    debug = require('debug')('kelladata-api')
;

/**
 * Get files by glob patterns
 */
var getGlobbedPaths = function (globPatterns, excludes) {
    // URL paths regex
    var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

    // The output array
    var output = [];

    // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
    if (_.isArray(globPatterns)) {
      globPatterns.forEach(function (globPattern) {
        output = _.union(output, getGlobbedPaths(globPattern, excludes));
      });
    } else if (_.isString(globPatterns)) {
      if (urlRegex.test(globPatterns)) {
        output.push(globPatterns);
      } else {
        var files = glob.sync(globPatterns);
        if (excludes) {
          files = files.map(function (file) {
            if (_.isArray(excludes)) {
              for (var i in excludes) {
                if (excludes.hasOwnProperty(i)) {
                  file = file.replace(excludes[i], '');
                }
              }
            } else {
              file = file.replace(excludes, '');
            }
            return file;
          });
        }
        output = _.union(output, files);
      }
    }

    return output;
  };

var initGlobalConfigFiles = (config, assets) => {
    config.files = {};

    // Setting Globbed model files
    config.files.models = getGlobbedPaths(assets.models);
    // debug("\n\nMODELS FILES");
    // debug(config.files.models);

    // Setting Globbed route files
    config.files.routes = getGlobbedPaths(assets.routes);
    // debug("\n\nROUTES FILES");
    // debug(config.files.routes);

    // Setting Globbed config files
    config.files.configs = getGlobbedPaths(assets.config);
    // debug("\n\nCONFIGS FILES");
    // debug(config.files.routes);


    // Setting Globbed socket files
    config.files.sockets = getGlobbedPaths(assets.sockets);
    // debug("\n\nSERVERS FILES");
    // debug(config.files.sockets);
}

var initGlobalConfig = () => {
    // Get the default and the current assets
    var defaultAssets = require(path.join(process.cwd(), 'config/assets/default'));
    var environmentAssets = require(path.join(process.cwd(), 'config/assets/', process.env.NODE_ENV)) || {};

    var assets = _.merge(defaultAssets, environmentAssets);

    // Get the default and the current config
    var defaultConfig = require(path.join(process.cwd(), 'config/env/default'));
    var environmentConfig = require(path.join(process.cwd(), 'config/env/', process.env.NODE_ENV)) || {};

    var config = _.merge(defaultConfig, environmentConfig);

    // Initialize global globbed files
    initGlobalConfigFiles(config, assets);


    config.utils = {
      getGlobbedPaths: getGlobbedPaths,
    };

    return config;
}

/**
 * Set configuration object
 */
module.exports = initGlobalConfig();
