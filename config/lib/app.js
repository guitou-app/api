'use strict'

var express = require('./express'),
	chalk = require('chalk'),

	mongooseService = require('./mongoose'),

	rabbitmq = require('./rabbitmq'),
	express = require('./express'),
	config = require('../config');

module.exports.init = async function (callback) {
	await rabbitmq.init();

	
	try {
		mongooseService.connect(function (db) {
			mongooseService.loadModels();

				var app = express.init(db);
				if (callback) callback(app, db, config);
				
			});

	} catch {
		throw new Error('Got an error with RABBITMQ...');
	}
		
}

module.exports.start = (callback) => {
	var _this = this;

	_this.init(function (app, db, config) {
		app.listen(config.port, config.host, function() {
			// Create server URL
			var server = (process.env.NODE_ENV === 'secure' ? 'https://' : 'http://') + config.host + ':' + config.port;
			// Logging initialization
			console.log('--');
			console.log(chalk.green(config.app.title));
			console.log();
			console.log(chalk.green('Environment:     ' + process.env.NODE_ENV));
			console.log(chalk.green('Server:          ' + server));
			console.log(chalk.green('Database:        ' + config.db.uri));
			console.log('--');

			if (callback) callback(app, db, config);
		});
	});
}
