'use strict';

// Load the module dependencies
var _ = require('lodash'),
  chalk = require('chalk'),
  glob = require('glob'),
  // config = require('../config'),
  path = require('path'),
  config = require(path.resolve('./config/config')),
  path = require('path'),
  fs = require('fs'),
  http = require('http'),
  https = require('https'),
  passport = require('passport'),
  socketio = require('socket.io'),
  jwt = require('jsonwebtoken'),
  socketioJwt = require('socketio-jwt'),
  debug = require('debug')('socket.io');

  /**
   * Get files by glob patterns
   */
var getGlobbedPaths = function (globPatterns, excludes) {
    // URL paths regex
    var urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i');

    // The output array
    var output = [];

    // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
    if (_.isArray(globPatterns)) {
      globPatterns.forEach(function (globPattern) {
        output = _.union(output, getGlobbedPaths(globPattern, excludes));
      });
    } else if (_.isString(globPatterns)) {
      if (urlRegex.test(globPatterns)) {
        output.push(globPatterns);
      } else {
        var files = glob.sync(globPatterns);
        if (excludes) {
          files = files.map(function (file) {
            if (_.isArray(excludes)) {
              for (var i in excludes) {
                if (excludes.hasOwnProperty(i)) {
                  file = file.replace(excludes[i], '');
                }
              }
            } else {
              file = file.replace(excludes, '');
            }
            return file;
          });
        }
        output = _.union(output, files);
      }
    }

    return output;
};

// Define the Socket.io configuration method
module.exports = function(app) { // function (app, db) {
  var server;

  server = http.createServer(app);
  // server.listen(config.port);

  // Create a new Socket.io server
  var io = socketio.listen(server);
  // var io = socketio(server);

  // var defaultAssets = require(path.join(process.cwd(), 'config/assets/default'));
  // var sockets = getGlobbedPaths("sockets/**/*.js")
  // debug('SOCKETS LIST');
  // debug(sockets);

  // Add an event listener to the 'connection' event
  io.use(socketioJwt.authorize({
    secret: config.jwt.secret,
    handshake: true,
    timeout: 15000,
    callback: 5000,
    algorithm: 'HS256'
  }));

  // debug('\n****    JWT SECRET --- ', config.jwt.secret, '\n\n');

  io.on(
    'connection',
    function (socket, callback) {
      // debug("\n\nConnected to the socket -- ")
      // debug(socket.decoded_token);

      // debug("QUERY : ")
      // debug(socket.handshake.query);

      socket.on('disconnect', function(reason) {
        // debug("\n----------*************===================************--------------");
        // debug("\t\t\tUser is disconnecting");
        // debug(`${reason}`);
        if (reason === "io server disconnect") {
          socket.connect();
        }
        // debug("------------*************===================************--------------\n");
      });

      socket.on('error', (error) => {

        debug("An error occured");
        debug(error);
      });

      // sockets.forEach(function (socketConfiguration) {
      config.files.sockets.forEach(function (socketConfiguration) {
        require(path.resolve(socketConfiguration))(io, socket);
      });
    }
  );

  app.io = io;

  return server;
};


// io.on(
//   'connection',
//   socketioJwt.authorize({
//     timeout: 15000,
//     callback: 5000,
//     secret: config.jwt.secret,
//     decodedPropertyName: 'my_decoded_token',
//   }),
// );
// //
// io.on('authenticated', (socket) => {
//   console.log('\n\nAUTHENTICATED ... ', socket.my_decoded_token, '\n\n'); // new decoded token
//
//   socket.on('disconnect', function(reason) {
//     debug("\n----------*************===================************--------------");
//     debug("\t\t\tUser is disconnecting");
//     debug(`${reason}`);
//     if (reason === "io server disconnect") {
//       socket.connect();
//     }
//     debug("------------*************===================************--------------\n");
//   });
//
//   socket.on('error', (error) => {
//
//     debug("An error occured");
//     debug(error);
//   });
// });

// io.sockets.on('connection', socketioJwt.authorize({
//   secret: config.jwt.secret,
//   // No client-side callback, terminate connection server-side
//   callback: false
// }))


//   io.use(function(socket, next) {
//     var handshake = socket.handshake.query;
// //     code: "invalid_token"
// // message: "invalid token"
// // type: "UnauthorizedError
//     var decoded;
//     debug('\n\nHANDSHAKE --- \n', handshake.token, '\n');
//     try {
//         // decoded = jwt.decode(handshake.token, 'Guitou');
//         jwt.verify(handshake.token, config.jwt.secret, (err, decoded) => {
//           if (err) {
//             console.error(err);
//             next({
//               success: false,
//               message: 'Token is not valid'
//             });
//           } else {
//             debug("\n\nUSE .. CHECK TOKEN .... ");
//             debug(decoded);
//             // req.user = decoded;
//             next();
//           }
//         });
//     } catch (err) {
//         console.error(err);
//         next(new Error('Invalid token!'));
//     }
//     console.log('DECODED - ', decoded, '\n\n');
//
//     if (decoded) {
//         // everything went fine - save userId as property of given connection instance
//         socket.userId = decoded.userId; // save user id we just got from the token, to be used later
//         next();
//     } else {
//         // invalid token - terminate the connection
//         next(new Error('Invalid token!'));
//     }
//   });
