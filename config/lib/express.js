'use strict';

var express = require('express')
var cors = require('cors')
var morgan = require('morgan')
var bodyParser = require('body-parser')
var path = require('path');
var config = require('../config');
var http = require('http');
var jwt = require('jsonwebtoken');
var debug = require('debug')('Guitou');
var requestIp = require('request-ip');

var checkToken = function(req, res, next) {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  debug("Check TOKEN .... " + token);
  if (token && token.startsWith('Bearer ')) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }
  debug(token);

  if (token) {
    jwt.verify(token, config.jwt.secret, (err, decoded) => {
      if (err) {
        return res.status(401).json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
      debug("CHECK TOKEN .... ")
      debug(decoded);
        req.user = decoded;
        next();
      }
    });
  } else {
    debug(req.headers);
    return res.status(411).json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
}

module.exports.initMiddleware = (app) => {
  app.use(function(req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
  });

  // app.use(cors({
  //   origin: ['*'], // ['https://DEMO.example.in'],
  //   methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  //   allowedHeaders: ['Content-Type', 'Authorization', 'Origin', 'x-access-token', 'XSRF-TOKEN'],
  //   preflightContinue: false
  // }));
  app.use(cors());

  app.use(requestIp.mw())
  app.use(function(req, res, next) {
      // by default, the ip address will be set on the `clientIp` attribute
      var ip = req.clientIp;
      // res.end(ip + '\n');
      debug("IP ADDRESS ", req.clientIp);
      next();
  });
  

  // app.use(cors());

  app.use(morgan('combined'));
  app.use(bodyParser.urlencoded({
      extended: true
  }));
  app.use(bodyParser.json());
};

module.exports.initModulesClientRoute = function(app) {
	// app.use('/', express.static(path.resolve('/public'), { maxAge: 86400000}));
	app.use('/uploads', express.static(path.resolve('./uploads'), { maxAge: 86400000}));
}

/**
 * Invoke modules server configuration
 */
module.exports.initModulesConfiguration = function (app) {
  config.files.configs.forEach(function (configPath) {
    require(path.resolve(configPath))(app);
  });
};

/**
 * Configure routes
 */
module.exports.initRoutes = function(app) {

  app.all('/api/a/*', checkToken);
  config.files.routes.forEach(function(routePath) {
		require(path.resolve(routePath))(app);
	});
};

/**
 * Configure Socket.io
 */
module.exports.configureSocketIO = function (app) { //}, db) {
  // Load the Socket.io configuration
  var server = require('./socket.io')(app);

  // Return server object
  return server;
};

/**
 * Initialize the Express application
 */
module.exports.init = function(db) {
  var app = express();

  this.initMiddleware(app);
  this.initModulesClientRoute(app);
  this.initRoutes(app);
  this.initModulesConfiguration(app);


  // app.all('/api/a/*', checkToken);
  app = this.configureSocketIO(app); //http.createServer(app);

  return app;
}
