// const amqp = require('amqplib/callback_api');
const amqp = require('amqplib');
const debug = require('debug')("guitou:rabbitmq");
const config = require('../config');

const queues = Object.values(config.rabbitmq.queue);
debug('RABBITMQ - QUEUES ', queues);

let connection, channel;

const init = async () => {
  connection = await amqp.connect(config.rabbitmq.uri); // || 'amqp://localhost')
  channel = await connection.createChannel();

  for (let i=0; i < queues.length; i++) {
    await channel.assertQueue(queues[i], { durable: true });
  }
}

const publishToQueue = async (queue, data) => {
  if (!connection) {
    await init();
  }

  await channel.assertQueue(queue, {durable: true});
  const buffer = Buffer.from(data);
  channel.sendToQueue(queue, buffer, {
    persistent: true
  });
}
 
module.exports = {
  init,
  publishToQueue
}
// let ch = null;
// amqp.connect(config.rabbitmq.uri, function (err, conn) {
//    conn.createChannel(function (err, channel) {
//       ch = channel;
//    });
// });

// exports.publishToQueue = async (queueName, data) => {
//   const buffer = Buffer.from(data);
//   ch.sendToQueue(queueName, buffer, {
//     persistent: true
//   });
// }


// process.on('exit', (code) => {
//   ch.close();
//   console.log(`Closing rabbitmq channel`);
// });

