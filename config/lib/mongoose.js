'use strict';

var _ = require('lodash'),
    path = require('path'),
    chalk = require('chalk'),
    mongoose = require('mongoose'),
    debug = require('debug')('guitou:mongoose'),

    config = require('../config')
;

/**
 * Load the mongoose models
 * @param {} callback
 */
module.exports.loadModels = function(callback) {
    config.files.models.forEach(function(modelPath) {
        require(path.resolve(modelPath));
    });

    if (callback) callback();
};

/**
 * Initialise Mongoose
 */
module.exports.connect = function(callback) {
  mongoose.Promise = config.db.promise;

  var options = _.merge(config.db.options || {}) ;

  console.log(config.db);
  console.log("CONFIG DBBBB");
  
  mongoose.set('toJSON', { virtuals: true });
  mongoose
    .connect(config.db.uri, options)
    .then(function(connection) {
      // Enabling debug mode if required
      mongoose.set('debug', config.db.debug);

      // Call callback function
      if (callback) callback(connection.db);
    })
    .catch(function(err) {
      console.error(chalk.red('Could not connect to MongoDB'));
      console.error(err);
    });
};

/**
 * Disconnection
 */
module.exports.disconnect = function (cb) {
  mongoose.connection.db
    .close(function (err) {
        console.info(chalk.yellow('Disconnect from MongoDB'));
        return cb(err);
    });
};
