var amqp = require('amqplib/callback_api');

var dataUpdateChannel = false;
amqp.connect('amqp://localhost', function(error, connection) {
  if (error) {
    throw error;
  }

  connection.createChannel(function(error, channel) {
    if (error) {
      throw error;
    }

    var queue = 'guitou.data.update';
    channel.assertQueue(queue, {
      durable: true
    });

    dataUpdateChannel = channel;
  });
});

module.exports = dataUpdateChannel;
