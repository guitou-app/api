var amqp = require('amqplib/callback_api');

var dataSaveChannel = false;
amqp.connect('amqp://localhost', function(error, connection) {
  if (error) {
    throw error;
  }

  connection.createChannel(function(error, channel) {
    if (error) {
      throw error;
    }

    var queue = 'guitou.data';
    channel.assertQueue(queue, {
      durable: true
    });

    dataSaveChannel = channel;
  });
});


module.exports = dataSaveChannel;
