'use strict';

var path = require('path'),
    config = require(path.resolve('./config/config'));

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var promise = mongoose.connect(config.db.uri, {
  useMongoClient: true
});
