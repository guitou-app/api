var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
var User = require('../../../models/user.model');
var debug = require('debug')('nkapsi');


// module.exports = function(passport){
module.exports = function(config){

  passport.serializeUser(function(user, done) {
    console.log('\n\nPSerialize: ' + user);

    done(null, user.id);
  }); 

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  console.log("Passport LocalStrategy");

  passport.use('local', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  }, function(username, password, done) {
    debug("Before nextTick");
    debug(username);
    debug(password);
    User.findUniquePhoneOrEmail(username.trim(), username.trim(), '', function(err, user) {
      if (err) {
        return done(err);
      }
      debug("Into");
      debug(user);
      if (!user || !user.authenticate(password)) {
        debug("Invalllllllllllliiiiiiiiiiiddddddddddddd up");
        debug(user);
        return done(null, false, {
          message: "Invalid username or password"
        });
      }

      // return done(null, user);
      debug("AFT ERRR INNNOTTTOOOO");
      debug(user);
      return done(null, user, {});
    });
  }))
}
